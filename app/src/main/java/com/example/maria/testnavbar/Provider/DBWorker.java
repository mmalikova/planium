package com.example.maria.testnavbar.Provider;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class DBWorker extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 20;
    private static final String DATABASE_NAME = "todo_db";
    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    Context appContext;

    public DBWorker(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.appContext = context;
    }

    // Create DB Structure
    @Override
    public void onCreate(SQLiteDatabase db) {
        String SQL_CREATE_ENTRIES = "CREATE TABLE 'task' (" +
                "_id             integer            PRIMARY KEY AUTOINCREMENT," +
                "sID             integer            NULL," +
                "userID          integer            NOT NULL," +
                "checked         integer            NULL," +
                "text            varchar(500)       NOT NULL," +
                "dueDate         INTEGER           NULL," +
                "notice          INTEGER           NULL," +
                "repeatID        integer            NULL," +
                "locationNotice  integer            NULL, " +
                "lastChange      INTEGER           NOT NULL);";
        db.execSQL(SQL_CREATE_ENTRIES);

        SQL_CREATE_ENTRIES = "CREATE TABLE 'user' (" +
                "_id            integer             PRIMARY KEY AUTOINCREMENT," +
                "sID            integer             NULL," +
                "login          varchar(100)        NOT NULL," +
                "password       varchar(100)        NOT NULL," +
                "email          varchar(100)        NOT NULL," +
                "active         integer             NOT NULL," +
                "lastChange     INTEGER             NULL);";
        db.execSQL(SQL_CREATE_ENTRIES);

        SQL_CREATE_ENTRIES = "CREATE TABLE 'context' (" +
                "_id            integer             PRIMARY KEY AUTOINCREMENT," +
                "sID            integer             NULL," +
                "userID         integer             NOT NULL," +
                "title          varchar(100)        NOT NULL," +
                "lastChange     INTEGER             NULL);";
        db.execSQL(SQL_CREATE_ENTRIES);

        SQL_CREATE_ENTRIES = "CREATE TABLE 'point' (" +
                "_id            integer             PRIMARY KEY AUTOINCREMENT," +
                "contextID      integer             NULL, " +
                "sID            integer             NULL," +
                "description    varchar(100)        NOT NULL," +
                "lat            varchar(100)        NOT NULL," +
                "lng            varchar(100)        NOT NULL, " +
                "lastChange     INTEGER             NULL);";
        db.execSQL(SQL_CREATE_ENTRIES);

        /*SQL_CREATE_ENTRIES = "CREATE TABLE 'keyword' (" +
                "_id            integer             PRIMARY KEY AUTOINCREMENT," +
                "sID            integer             NULL," +
                "contextID      integer             NOT NULL," +
                "word           varchar(100)        NOT NULL," +
                "lastChange     DATETIME            NOT NULL);";
        db.execSQL(SQL_CREATE_ENTRIES);*/

        SQL_CREATE_ENTRIES = "CREATE TABLE 'task_context'(" +
                "_id            integer             PRIMARY KEY AUTOINCREMENT," +
                "taskID         integer             NOT NULL," +
                "contextID      integer             NOT NULL)";
        db.execSQL(SQL_CREATE_ENTRIES);

        SQL_CREATE_ENTRIES = "CREATE TABLE 'subtask' (" +
                "_id            integer             PRIMARY KEY NOT NULL," +
                "sID            integer             NULL," +
                "taskID         integer             NOT NULL," +
                "text           varchar(500)        NOT NULL," +
                "lastChange     INTEGER             NULL)";
        db.execSQL(SQL_CREATE_ENTRIES);



    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS 'task'");
        db.execSQL("DROP TABLE IF EXISTS 'user'");
        db.execSQL("DROP TABLE IF EXISTS 'context'");
        db.execSQL("DROP TABLE IF EXISTS 'point'");
        db.execSQL("DROP TABLE IF EXISTS 'subtask'");
        db.execSQL("DROP TABLE IF EXISTS 'task_context'");
        onCreate(db);
    }

    // ------------------------------------------------------------------------------------------
    // Work with User

    // check auth user
    public UserContext checkUser(){
        UserContext user = new UserContext();
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM 'user' WHERE (active = '1') limit 1";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0){
            cursor.moveToFirst();
            user = new UserContext();
            user.setID(cursor.getInt(0));
            user.setsID(cursor.getInt(1));
            user.setLogin(cursor.getString(2));
            user.setPassword(cursor.getString(3));
            user.setEmail(cursor.getString(4));
            user.setActive(cursor.getInt(5));
            //user.setLastChange(cursor.getInt(6));
            return user;
        }
        else{
            return user;
        }
    }

    // авторизация пользователя
    public UserContext authUser(String email, String pass){
        // TODO авторизация на стороне сервера
        UserContext user = new UserContext();
        Calendar cal = Calendar.getInstance();
        long curDate = cal.getTime().getTime()/1000;
        //проверяем, есть ли пользователь в базе
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM 'user' WHERE (email = '" + email + "' and password = '"+  pass +"')";
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0){
            //if (cursor != null) {
            cursor.moveToFirst();
            user = new UserContext();
            user.setID(cursor.getInt(0));
            user.setsID(cursor.getInt(1));
            user.setLogin(cursor.getString(2));
            user.setPassword(cursor.getString(3));
            user.setEmail(cursor.getString(4));
            user.setActive(cursor.getInt(5));
            // обновляем этого пользователя - ставим ему активный статус
        }

        db.close();

        if (user.getLogin() != null) {
            SQLiteDatabase db1 = this.getWritableDatabase();
            ContentValues newValues = new ContentValues();
            newValues.put("login", user.getLogin());
            newValues.put("password", user.getPassword());
            newValues.put("email", user.getEmail());
            newValues.put("active", 1);
            newValues.put("lastChange", curDate);
            String where = "_id =" + user.getID();
            db1.update("user", newValues, where, null);
            db1.close();
        }
        return user;
    }

    // регистрация пользователя
    public UserContext registerUser(UserContext user){
        Calendar cal = Calendar.getInstance();
        long curDate = cal.getTime().getTime()/1000;
        SQLiteDatabase db = this.getWritableDatabase();
        String insertQuery = "INSERT INTO 'user' ('sID','login', 'password', 'email', 'active', 'lastChange') VALUES ('"+user.getsID()+"','" + user.getLogin() + "', '" + user.getPassword() + "', '"+user.getEmail()+"', '1', 'curDate')";
        db.execSQL(insertQuery);

        String query = "SELECT * FROM 'user' order by _id DESC limit 1";
        Cursor cursor = db.rawQuery(query, null);
        int id = 0;

        if (cursor.moveToFirst()) {
            do {
                id = cursor.getInt(0);
            } while (cursor.moveToNext());
        }
        user.setID(id);

        db.close();
        return user;
    }

    public void Exit() {
        UserContext user = new UserContext();
        Calendar cal = Calendar.getInstance();
        long curDate = cal.getTime().getTime()/1000;
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM 'user' WHERE (active = '1') limit 1";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0){
            //if (cursor != null){
            cursor.moveToFirst();
            user = new UserContext();
            user.setID(cursor.getInt(0));
            user.setsID(cursor.getInt(1));
            user.setLogin(cursor.getString(2));
            user.setPassword(cursor.getString(3));
            user.setEmail(cursor.getString(4));
            user.setActive(cursor.getInt(5));
            //обновляем пользователя, ставим ему статус 0
        }
        db.close();

        if (user.getLogin()!=null){
            SQLiteDatabase db1 = this.getWritableDatabase();
            ContentValues newValues = new ContentValues();
            newValues.put("login", user.getLogin());
            newValues.put("password", user.getPassword());
            newValues.put("email", user.getEmail());
            newValues.put("active", 0);
            newValues.put("sID", user.getsID());
            newValues.put("lastChange", curDate);
            String where = "_id = '"+user.getID()+"'";
            db1.update("user", newValues, where, null);
            db1.close();
        }
    }



    // ------------------------------------------------------------------------------------------
    // Work with Task

    // add new task to db - return task id
    public int newTask(TaskContext task){
        SQLiteDatabase db = this.getWritableDatabase();
        long curDate = 0;
        if (task.getlChSec()>0){
            curDate = task.getlChSec();
        } else {
            Calendar cal = Calendar.getInstance();
            curDate = cal.getTime().getTime()/1000;
        }

        Long due = null;
        Long notice = null;
        if (task.getDueDate()!=null){
            due = task.getDueDate().getTime()/1000;
        }
        if (task.getNotice() != null) {
            notice = task.getNotice().getTime()/1000;
        }
        String insertQuery = "INSERT INTO 'task' " +
                "('userID', 'checked', 'text', 'dueDate', 'notice', 'repeatID', 'locationNotice', 'lastChange', 'sID') " +
                "VALUES " +
                "('" +task.getUserID() + "', " +
                "'" + task.getChecked() + "', " +
                "'" + task.getText() + "', " +
                "'" + due + "', " +
                "'" + notice + "', " +
                "'" + task.getRepeatID() + "', " +
                "'" + task.getLocationNotice() + "', " +
                "'" + curDate + "'," +
                "'" + task.getsID() + "')";
        db.execSQL(insertQuery);

        String query = "SELECT * FROM 'task' order by _id DESC limit 1";
        Cursor cursor = db.rawQuery(query, null);
        int id = 0;

        if (cursor.moveToFirst()) {
            do {
                id = cursor.getInt(0);
            } while (cursor.moveToNext());
        }
        task.setID(id);
        ///
        //addNoteLocations(Note);
        db.close();
        return task.getID();
    }

    // change existing task
    public void changeTask(TaskContext task) {
        SQLiteDatabase db = this.getWritableDatabase();
        Calendar cal = Calendar.getInstance();
        long curDate = cal.getTime().getTime()/1000;

        ContentValues newValues = new ContentValues();
        Long due = null;
        Long notice = null;
        if (task.getDueDate()!=null){
            due = task.getDueDate().getTime()/1000;
        }
        if (task.getNotice() != null) {
            notice = task.getNotice().getTime()/1000;
        }

        // Назначьте значения для каждой строки.
        newValues.put("sID ", task.getsID());
        newValues.put("userID", task.getUserID());
        newValues.put("checked", task.getChecked());
        newValues.put("text", task.getText());

        newValues.put("dueDate", due);
        newValues.put("notice", notice);
        newValues.put("repeatID", task.getRepeatID());
        newValues.put("locationNotice", task.getLocationNotice());
        newValues.put("lastChange", curDate);

        String where = "_id =" + task.getID();
        db.update("task", newValues, where, null);


        db.close();
    }

    //delete task from db
    public void deleteTask(TaskContext task) {
        SQLiteDatabase db = this.getWritableDatabase();
        String deleteQuery = "DELETE FROM 'task' WHERE userID = '"+task.getUserID()+"' and _id = '"+task.getID()+"'";
        db.execSQL(deleteQuery);

        db.close();
    }

    public void deleteTask(int taskID, int userID) {
        SQLiteDatabase db = this.getWritableDatabase();
        String deleteQuery = "DELETE FROM 'task' WHERE userID = '"+taskID+"' and _id = '"+userID+"'";
        db.execSQL(deleteQuery);

        db.close();
    }

    // get all Users task
    public ArrayList<TaskContext> getAllTasks(UserContext user){
        ArrayList<TaskContext> taskList = new ArrayList<TaskContext>();
        //String selectQuery = "SELECT  * FROM notes where userID = '"+Integer.toString(id)+"'";
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(appContext);
        boolean done = prefs.getBoolean("done_tasks", false);
        String selectQuery;
        if (done){
            selectQuery = "SELECT  * FROM task where userID = '"+user.getID()+"' order by dueDate";
        } else{
            selectQuery = "SELECT  * FROM task where userID = '"+user.getID()+"' and checked <> '1' order by dueDate";
        }



        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        TaskContext task;

        if (cursor.moveToFirst()) {
            do {
                task  = new TaskContext();
                task.setID(cursor.getInt(0));
                task.setsID(cursor.getInt(1));
                task.setUserID(cursor.getInt(2));
                task.setChecked(cursor.getInt(3));
                task.setText(cursor.getString(4));
                Date dt = new Date(cursor.getLong(5)*1000);
                task.setDueDate(dt);
                // task.setNotice
                task.setRepeatID(cursor.getInt(7));
                task.setLocationNotice(cursor.getInt(8));
                // task.setLastChange
                taskList.add(task);
            } while (cursor.moveToNext());
        }
        db.close();
        return taskList;
    }

    // get task by id
    public TaskContext getTaskById (int id){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM 'task' WHERE _id = '" + id + "'";
        Cursor cursor = db.rawQuery(query, null);

        TaskContext task = new TaskContext();

        if (cursor.moveToFirst()) {
            do {
                task  = new TaskContext();
                task.setID(cursor.getInt(0));
                task.setsID(cursor.getInt(1));
                task.setUserID(cursor.getInt(2));
                task.setChecked(cursor.getInt(3));
                task.setText(cursor.getString(4));
                // TODO read date from db
                Date dt = new Date(cursor.getLong(5)*1000);
                task.setDueDate(dt);
                dt = new Date(cursor.getLong(6)*1000);
                task.setNotice(dt);
                task.setRepeatID(cursor.getInt(7));
                task.setLocationNotice(cursor.getInt(8));
                // task.setLastChange
            } while (cursor.moveToNext());
        }
        //db.close();
        return task;
    }

    public TaskContext getUncheckedTaskById (int id){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM 'task' WHERE _id = '" + id + "' and checked <> '1'";
        Cursor cursor = db.rawQuery(query, null);

        TaskContext task = new TaskContext();

        if (cursor.moveToFirst()) {
            do {
                task  = new TaskContext();
                task.setID(cursor.getInt(0));
                task.setsID(cursor.getInt(1));
                task.setUserID(cursor.getInt(2));
                task.setChecked(cursor.getInt(3));
                task.setText(cursor.getString(4));
                // TODO read date from db
                Date dt = new Date(cursor.getLong(5)*1000);
                task.setDueDate(dt);
                dt = new Date(cursor.getLong(6)*1000);
                task.setNotice(dt);
                task.setRepeatID(cursor.getInt(7));
                task.setLocationNotice(cursor.getInt(8));
                // task.setLastChange
            } while (cursor.moveToNext());
        }
        //db.close();
        return task;
    }

    public TaskContext getNotifyTaskById (int id){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM 'task' WHERE _id = '" + id + "' and locationNotice = 1";
        Cursor cursor = db.rawQuery(query, null);

        TaskContext task = new TaskContext();

        if (cursor.moveToFirst()) {
            do {
                task  = new TaskContext();
                task.setID(cursor.getInt(0));
                task.setsID(cursor.getInt(1));
                task.setUserID(cursor.getInt(2));
                task.setChecked(cursor.getInt(3));
                task.setText(cursor.getString(4));
                // TODO read date from db
                Date dt = new Date(cursor.getLong(5)*1000);
                task.setDueDate(dt);
                dt = new Date(cursor.getLong(6)*1000);
                task.setNotice(dt);
                task.setRepeatID(cursor.getInt(7));
                task.setLocationNotice(cursor.getInt(8));
                // task.setLastChange
            } while (cursor.moveToNext());
        }
        //db.close();
        return task;
    }

    public ArrayList<TaskContext> getTasksByContext(int contextID){
        ArrayList<TaskContext> tasks = new ArrayList<TaskContext>();
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM 'task_context' WHERE contextID = '" + contextID + "'";
        Cursor cursor = db.rawQuery(query, null);

        TaskContext task = new TaskContext();
        int taskID;

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(appContext);
        boolean done = prefs.getBoolean("done_tasks", false);
        String selectQuery;


        if (cursor.moveToFirst()) {
            do {
                taskID = cursor.getInt(1);

                if (done){
                    task = getUncheckedTaskById(taskID);
                } else{
                    task = getTaskById(taskID);
                }
                tasks.add(task);
            } while (cursor.moveToNext());
        }

        return tasks;
    }

    public ArrayList<TaskContext> getNotifyTasksByContext(int contextID){
        ArrayList<TaskContext> tasks = new ArrayList<TaskContext>();
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM 'task_context' WHERE contextID = '" + contextID + "'";
        Cursor cursor = db.rawQuery(query, null);

        TaskContext task = new TaskContext();
        int taskID;

        if (cursor.moveToFirst()) {
            do {
                taskID = cursor.getInt(1);
                task = getNotifyTaskById(taskID);
                if (task.getText() != null)
                    tasks.add(task);
            } while (cursor.moveToNext());
        }

        return tasks;
    }

    public ArrayList<TaskContext> getTasksWithoutContext(UserContext user){
        ArrayList<TaskContext> tasks = new ArrayList<TaskContext>();
        ArrayList<TaskContext> res = new ArrayList<TaskContext>();
        tasks = getAllTasks(user);
        SQLiteDatabase db = this.getReadableDatabase();
        String query;
        Cursor cursor;
        for (int i = 0; i<tasks.size(); i++){
            query = "SELECT * FROM 'task_context' WHERE taskID = '"+tasks.get(i).getID()+"'";
            cursor = db.rawQuery(query, null);
            if (!cursor.moveToFirst()) {
                res.add(tasks.get(i));
            }
        }
        return res;
    }

    public Integer getTotalTasksCount(int userID){
        Integer cnt = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM 'task' WHERE userID = '" + userID + "' and checked <>  '"+1+"'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                cnt++;
            } while (cursor.moveToNext());
        }
        return cnt;
    }

    public Integer getTodayTasksCount(int userID){
        Integer cnt = 0;
        SQLiteDatabase db = this.getReadableDatabase();

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        Date date = cal.getTime();
        Long date1 = date.getTime()/1000;

        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE,59);
        date = cal.getTime();
        Long date2 = date.getTime()/1000;

        String query = "SELECT * FROM 'task' WHERE userID = '" + userID + "' and checked <>  '"+1+"' and (dueDate > '"+date1+"' and dueDate < '"+date2+"')";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                cnt++;
            } while (cursor.moveToNext());
        }
        return cnt;
    }

    public ArrayList<TaskContext> getTodayTasks(int userID){
        ArrayList<TaskContext> taskList = new ArrayList<TaskContext>();
        Integer cnt = 0;
        SQLiteDatabase db = this.getReadableDatabase();

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        Date date = cal.getTime();
        Long date1 = date.getTime()/1000;

        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE,59);
        date = cal.getTime();
        Long date2 = date.getTime()/1000;


        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(appContext);
        boolean done = prefs.getBoolean("done_tasks", false);
        String query;
        if (done){
            query = "SELECT * FROM 'task' WHERE userID = '" + userID + "' and (dueDate >= '"+date1+"' and dueDate <= '"+date2+"')";
        } else{
            query = "SELECT * FROM 'task' WHERE userID = '" + userID + "' and checked <>  '"+1+"' and (dueDate >= '"+date1+"' and dueDate <= '"+date2+"')";
        }
        Cursor cursor = db.rawQuery(query, null);
        TaskContext task;
        if (cursor.moveToFirst()) {
            do {
                task  = new TaskContext();
                task.setID(cursor.getInt(0));
                task.setsID(cursor.getInt(1));
                task.setUserID(cursor.getInt(2));
                task.setChecked(cursor.getInt(3));
                task.setText(cursor.getString(4));
                // TODO read date from db
                // task.setDueDate(cursor.get);
                // task.setNotice
                task.setRepeatID(cursor.getInt(7));
                task.setLocationNotice(cursor.getInt(8));
                // task.setLastChange
                taskList.add(task);
            } while (cursor.moveToNext());
        }
        return taskList;
    }

    public Integer getWeekTasksCount(int userID){
        Integer cnt = 0;
        SQLiteDatabase db = this.getReadableDatabase();


        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE,0);

        Date date = cal.getTime();
        Long date1 = date.getTime()/1000;


        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE,59);
        cal.add(Calendar.DATE, 7);
        date = cal.getTime();
        Long date2 = date.getTime()/1000;

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(appContext);
        boolean done = prefs.getBoolean("done_tasks", false);
        String query;
        if (done){
            query = "SELECT * FROM 'task' WHERE userID = '" + userID + "' and (dueDate > '"+date1+"' and dueDate < '"+date2+"')";
        } else{
            query = "SELECT * FROM 'task' WHERE userID = '" + userID + "' and checked <>  '"+1+"' and (dueDate > '"+date1+"' and dueDate < '"+date2+"')";
        }


        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                cnt++;
            } while (cursor.moveToNext());
        }
        return cnt;
    }

    public ArrayList<SeparatedTask> getSeparatedTask(int userID){

        ArrayList<SeparatedTask> SepTaskList = new ArrayList<SeparatedTask>();
        ArrayList<TaskContext> taskList = new ArrayList<TaskContext>();

        ///
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE,0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        Date date = cal.getTime();
        Date last_date = date;
        Long date1 = date.getTime()/1000;
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE,59);
        cal.add(Calendar.DATE, 7);
        date = cal.getTime();
        Long date2 = date.getTime()/1000;

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(appContext);
        boolean done = prefs.getBoolean("done_tasks", false);
        String selectQuery;
        if (!done){
            selectQuery = "SELECT  * FROM task where userID = '"+userID+"'  and checked <>  '"+1+"' and (dueDate > '"+date1+"' and dueDate < '"+date2+"') order by dueDate";
        } else{
            selectQuery = "SELECT  * FROM task where userID = '"+userID+"'  and (dueDate > '"+date1+"' and dueDate < '"+date2+"') order by dueDate";
        }
        ///


        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        TaskContext task;

        //Date last_date = текущему дню
        if (cursor.moveToFirst()) {
            do {
                task  = new TaskContext();
                task.setsID(cursor.getInt(1));
                task.setID(cursor.getInt(0));
                task.setChecked(cursor.getInt(3));
                task.setText(cursor.getString(4));
                Date dt = new Date(cursor.getLong(5)*1000);
                task.setDueDate(dt);
                taskList.add(task);
            } while (cursor.moveToNext());
        }
        db.close();

        SeparatedTask sTask;
        // перебираем данные и приводим их к нужному виду
        for (int i = 0; i<taskList.size(); i++){
            sTask = new SeparatedTask();
            sTask.setID(taskList.get(i).getID());
            sTask.setTitle(taskList.get(i).getText());
            sTask.setIsChecked(taskList.get(i).getChecked());
            if (i == 0) {
                sTask.setNeedSeparator(1);
                sTask.setSeparatorTitle(new SimpleDateFormat ( "EEEE" ).format ( taskList.get(i).getDueDate() ));
                last_date = taskList.get(i).getDueDate();

            }
            else {
                int rez = taskList.get(i).getDueDate().compareTo(last_date);
                if (rez > 0){
                    sTask.setNeedSeparator(1);
                    sTask.setSeparatorTitle(new SimpleDateFormat ( "EEEE" ).format ( taskList.get(i).getDueDate() ));
                    last_date = taskList.get(i).getDueDate();

                }
            }

            SepTaskList.add(sTask);
        }

        return SepTaskList;

    }

    public TaskContext getTaskLastChangeFromSID(TaskContext task){
        SQLiteDatabase db = this.getWritableDatabase();
        // check if context exist

        TaskContext ccc = new TaskContext();
        String query = "SELECT * FROM 'task' WHERE sID = '" + task.getsID() + "'";
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                ccc.setID(cursor.getInt(0));
                ccc.setsID(cursor.getInt(1));
                ccc.setUserID(cursor.getInt(2));
                ccc.setChecked(cursor.getInt(3));
                ccc.setText(cursor.getString(4));
                Date dt = new Date(cursor.getLong(5)*1000);
                ccc.setDueDate(dt);
                dt = new Date(cursor.getLong(6)*1000);
                ccc.setNotice(dt);
                ccc.setRepeatID(cursor.getInt(7));
                ccc.setLocationNotice(cursor.getInt(8));
                ccc.setlChSec(cursor.getInt(9));
            } while (cursor.moveToNext());
        }
        db.close();
        return ccc;
    }

    public void updateTaskFromServer(TaskContext task){
        long curDate;
        if (task.getlChSec()>0){
            curDate = task.getlChSec();
        } else {
            Calendar cal = Calendar.getInstance();
            curDate = cal.getTime().getTime() / 1000;
        }
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues newValues = new ContentValues();
        Long due = null;
        Long notice = null;
        if (task.getDueDate()!=null){
            due = task.getDueDate().getTime()/1000;
        }
        if (task.getNotice() != null) {
            notice = task.getNotice().getTime()/1000;
        }
        newValues.put("sID ", task.getsID());
        newValues.put("userID", task.getUserID());
        newValues.put("checked", task.getChecked());
        newValues.put("text", task.getText());

        newValues.put("dueDate", due);
        newValues.put("notice", notice);
        newValues.put("repeatID", task.getRepeatID());
        newValues.put("locationNotice", task.getLocationNotice());
        newValues.put("lastChange", curDate);


        String where = "sID =" + task.getsID();
        // Обновите строку с указанным индексом, используя новые значения.
        db.update("task", newValues, where, null);

        db.close();
    }

    public ArrayList<TaskContext> getAllUnsyncTask (UserContext user){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM 'task' WHERE sID = '0' and userID = '" + user.getID() + "'";
        Cursor cursor = db.rawQuery(query, null);
        ArrayList<TaskContext> list = new ArrayList<TaskContext>();
        TaskContext task;
        if (cursor.moveToFirst()) {
            do {
                task  = new TaskContext();
                task.setID(cursor.getInt(0));
                task.setsID(cursor.getInt(1));
                task.setUserID(cursor.getInt(2));
                task.setChecked(cursor.getInt(3));
                task.setText(cursor.getString(4));
                Date dt = new Date(cursor.getLong(5)*1000);
                task.setDueDate(dt);
                dt = new Date(cursor.getLong(6)*1000);
                task.setNotice(dt);
                task.setRepeatID(cursor.getInt(7));
                task.setLocationNotice(cursor.getInt(8));
                task.setlChSec(cursor.getInt(9));
                list.add(task);
            } while (cursor.moveToNext());
        }
        db.close();
        return list;

    }

    // ------------------------------------------------------------------------------------------
    // Work with subTask

    public ArrayList<SubtaskContext> getSubTaskListByTaskID (int taskID){
        ArrayList<SubtaskContext> subTaskList = new ArrayList<SubtaskContext>();
        //String selectQuery = "SELECT  * FROM notes where userID = '"+Integer.toString(id)+"'";
        String selectQuery = "SELECT  * FROM subtask where taskID = '"+taskID+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        SubtaskContext task;

        if (cursor.moveToFirst()) {
            do {
                task  = new SubtaskContext();
                task.setID(cursor.getInt(0));
                task.setsID(cursor.getInt(1));
                task.setTaskID(cursor.getInt(2));
                task.setText(cursor.getString(3));
                // task.setLastChange
                subTaskList.add(task);
            } while (cursor.moveToNext());
        }
        db.close();
        return subTaskList;
    }

    public void addSubTask(SubtaskContext subtask){
        Calendar cal = Calendar.getInstance();
        long curDate = cal.getTime().getTime()/1000;
        SQLiteDatabase db = this.getWritableDatabase();
        String insertQuery = "INSERT INTO 'subtask' " +
                "('taskID', 'text', 'lastChange') " +
                "VALUES " +
                "('" +subtask.getTaskID() + "', " +
                "'" + subtask.getText() + "', " +
                "'" + curDate + "')";
        db.execSQL(insertQuery);
        db.close();
    }

    public void removeSubTaskByID(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        String deleteQuery = "DELETE FROM 'subtask' WHERE _id = '"+id+"'";
        db.execSQL(deleteQuery);
        db.close();
    }

    public void updateSubTask(SubtaskContext subtask){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues newValues = new ContentValues();
        newValues.put("taskID", subtask.getTaskID());
        newValues.put("text", subtask.getText());
        String where = "_id =" + subtask.getID();
        db.update("point", newValues, where, null);

        db.close();
    }

    public void saveAllSubtaskToTask(ArrayList<SubtaskContext> list, TaskContext task){
        SQLiteDatabase db = this.getWritableDatabase();
        Calendar cal = Calendar.getInstance();
        long curDate = cal.getTime().getTime()/1000;
        String query;
        Cursor cursor;
        int userID = task.getUserID();
        int id = task.getID();

        //delete all recent subtasksate
        String deleteQuery = "DELETE FROM 'subtask' WHERE taskID = '"+id+"'";
        db.execSQL(deleteQuery);


        for (int i = 0; i<list.size(); i++){
            if (!list.get(i).isChecked()){
                String insertQuery = "INSERT INTO 'subtask' " +
                        "('taskID', 'text', 'lastChange', 'sID') " +
                        "VALUES " +
                        "('" +id + "', " +
                        "'" + list.get(i).getText() + "', " +
                        "'" + curDate + "'," +
                        "'"+list.get(i).getsID()+"')";
                db.execSQL(insertQuery);
            }
        }
        db.close();
    }

    public void removeAllSubtasks(int taskID){
        SQLiteDatabase db = this.getWritableDatabase();
        String deleteQuery = "DELETE FROM 'subtask' WHERE taskID = '"+taskID+"'";
        db.execSQL(deleteQuery);
        db.close();
    }



    // ------------------------------------------------------------------------------------------
    // Work with Context
    public ContextContext getContextByPoint(double lat, double lng){
        ContextContext context = new ContextContext();

        SQLiteDatabase db = this.getReadableDatabase();


        String query = "SELECT * FROM point WHERE lat = '" + lat + "' and lng = '" + lng + "'";
        Cursor cursor = db.rawQuery(query, null);
        int contextID = 0;
        if (cursor.moveToFirst()) {
            do {
                contextID = cursor.getInt(1);
            } while (cursor.moveToNext());
        }

        context = getContextByID(contextID);

        return context;
    }

    public int addContext(ContextContext cnt){
        SQLiteDatabase db = this.getWritableDatabase();
        long curDate;
        if (cnt.getlChSec()>0) {
            curDate = cnt.getlChSec();
        } else {
            Calendar cal = Calendar.getInstance();
            curDate = cal.getTime().getTime() / 1000;
        }
        String insertQuery = "INSERT INTO 'context' " +
                "('userID', 'title', 'sID', 'lastChange') " +
                "VALUES " +
                "('" + cnt.getUserID() + "'," +
                "'" + cnt.getTitle() +"'," +
                "'" + cnt.getsID() + "'," +
                "'"+curDate+"')";
        db.execSQL(insertQuery);
        String query = "SELECT * FROM 'context' order by _id DESC limit 1";
        Cursor cursor = db.rawQuery(query, null);
        int id = 0;
        if (cursor.moveToFirst()) {
            do {
                id = cursor.getInt(0);
            } while (cursor.moveToNext());
        }
        db.close();
        return id;
    }

    public ArrayList<ContextContext> getAllContexts(int userID){

        ArrayList<ContextContext> contexts = new ArrayList<ContextContext>();
        SQLiteDatabase db = this.getWritableDatabase();


        String selectQuery = "SELECT  * FROM context where userID = '"+userID+"'";

        Cursor cursor = db.rawQuery(selectQuery, null);
        ContextContext context;

        if (cursor.moveToFirst()) {
            do {
                context  = new ContextContext();
                context.setID(cursor.getInt(0));
                context.setsID(cursor.getInt(1));
                context.setUserID(cursor.getInt(2));
                context.setTitle(cursor.getString(3));
                contexts.add(context);
            } while (cursor.moveToNext());
        }
        db.close();

        return contexts;
    }

    public ContextContext getContextByID(int cntID){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM 'context' WHERE _id = '" + cntID + "'";
        Cursor cursor = db.rawQuery(query, null);
        ContextContext context = new ContextContext();
        if (cursor.moveToFirst()) {
            do {
                context  = new ContextContext();
                context.setID(cursor.getInt(0));
                context.setsID(cursor.getInt(1));
                context.setUserID(cursor.getInt(2));
                context.setTitle(cursor.getString(3));
            } while (cursor.moveToNext());
        }
        db.close();
        return context;
    }

    public ContextContext getContextByTitle(String title, int userID){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM 'context' WHERE title = '" + title + "' and userID = '" + userID + "'";
        Cursor cursor = db.rawQuery(query, null);
        ContextContext context = new ContextContext();
        if (cursor.moveToFirst()) {
            do {
                context  = new ContextContext();
                context.setID(cursor.getInt(0));
                context.setsID(cursor.getInt(1));
                context.setUserID(cursor.getInt(2));
                context.setTitle(cursor.getString(3));
            } while (cursor.moveToNext());
        }
        db.close();
        return context;
    }

    public void removeContext(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        String deleteQuery = "DELETE FROM 'point' WHERE contextID = '"+id+"'";
        db.execSQL(deleteQuery);
        deleteQuery = "DELETE FROM 'context' WHERE _id = '"+id+"'";
        db.execSQL(deleteQuery);
        db.close();
    }

    public String[] getContextNameList(int userID){
        ArrayList<String> pointsList = new ArrayList<String>();

        String selectQuery = "SELECT  * FROM context where userID = '"+userID+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                pointsList.add(cursor.getString(3));
            } while (cursor.moveToNext());
        }
        db.close();

        String[] points = new String[pointsList.size()];
        for (int i = 0; i< pointsList.size(); i++)        {
            points[i] = pointsList.get(i);
        }
        return points;
    }

    public void saveTaskContext(ArrayList<String> titles, TaskContext task){
        SQLiteDatabase db = this.getWritableDatabase();

        String query;
        Cursor cursor;
        int userID = task.getUserID();
        int id = task.getID();

        // delete all recent relations
        String deleteQuery = "DELETE FROM 'task_context' WHERE taskID = '"+id+"'";
        db.execSQL(deleteQuery);

        if (titles != null) {
            if (titles.size()>0){
                // для каждой point получаем id
                int cntID = 0;
                for (int i = 0; i<titles.size(); i++) {
                    query = "SELECT * FROM 'context' WHERE title = '" + titles.get(i) +"' and userID = '"+userID+"'";
                    cursor = db.rawQuery(query, null);
                    if (cursor.moveToFirst()) {
                        do {
                            cntID = cursor.getInt(0);
                        } while (cursor.moveToNext());
                    }
                    // записываем в таблицу привязку к заметке
                    query = "INSERT into 'task_context' ('taskID', 'contextID') VALUES ('"+id+"', '"+cntID+"')";
                    db.execSQL(query);
                }
            }
        }
        db.close();
    }

    public boolean[] getSelectedContext(int taskID, int userID){

        ArrayList<ContextContext> allContexts = getAllContexts(userID);
        ArrayList<Integer> taskContextID = new ArrayList<Integer>();

        String selectQuery = "SELECT * FROM task_context where taskID = '"+taskID+"'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                taskContextID.add(cursor.getInt(2));
            } while (cursor.moveToNext());
        }

        boolean[] selLoc = new boolean[allContexts.size()];
        for (int i = 0; i<allContexts.size(); i++){
            if (taskContextID.contains(allContexts.get(i).getID())){
                selLoc[i] = true;
            }
            else {
                selLoc[i]= false;
            }
        }
        db.close();
        return selLoc;

    }

    public boolean[] getSelectedContextAuto(int userID, int serverContextID){

        ArrayList<ContextContext> allContexts = getAllContexts(userID);
        ArrayList<Integer> taskContextID = new ArrayList<Integer>();

        String selectQuery = "SELECT * FROM context where sID = '"+serverContextID+"'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                taskContextID.add(cursor.getInt(0));
            } while (cursor.moveToNext());
        }

        boolean[] selLoc = new boolean[allContexts.size()];
        for (int i = 0; i<allContexts.size(); i++){
            if (taskContextID.contains(allContexts.get(i).getID())){
                selLoc[i] = true;
            }
            else {
                selLoc[i]= false;
            }
        }
        db.close();
        return selLoc;

    }

    public void updateContext(ContextContext context){
        Calendar cal = Calendar.getInstance();
        long curDate = cal.getTime().getTime()/1000;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues newValues = new ContentValues();
        // Назначьте значения для каждой строки.
        //newValues.put("contextID ", point.getContextID());
        newValues.put("sID", context.getsID());
        newValues.put("userID", context.getUserID());
        newValues.put("title", context.getTitle());
        newValues.put("lastChange", curDate);


        String where = "_id =" + context.getID();
        // Обновите строку с указанным индексом, используя новые значения.
        db.update("context", newValues, where, null);

        db.close();
    }

    public void updateContextFromServer(ContextContext context){
        long curDate;
        if (context.getlChSec()>0){
            curDate = context.getlChSec();
        } else {
            Calendar cal = Calendar.getInstance();
            curDate = cal.getTime().getTime() / 1000;
        }
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues newValues = new ContentValues();
        // Назначьте значения для каждой строки.
        //newValues.put("contextID ", point.getContextID());
        newValues.put("_id", context.getID());
        newValues.put("userID", context.getUserID());
        newValues.put("title", context.getTitle());
        newValues.put("lastChange", curDate);


        String where = "sID =" + context.getsID();
        // Обновите строку с указанным индексом, используя новые значения.
        db.update("context", newValues, where, null);

        db.close();
    }

    public String getContextServerIDs(ArrayList<String> titles, UserContext user){
        SQLiteDatabase db = this.getWritableDatabase();
        String query;
        Cursor cursor;
        int userID = user.getID();
        ArrayList<Integer> ret = new ArrayList<Integer>();

        if (titles != null) {
            if (titles.size()>0){
                // для каждой point получаем id
                int cntSID = 0;
                for (int i = 0; i<titles.size(); i++) {
                    query = "SELECT * FROM 'context' WHERE title = '" + titles.get(i) +"' and userID = '"+userID+"'";
                    cursor = db.rawQuery(query, null);
                    if (cursor.moveToFirst()) {
                        do {
                            cntSID = cursor.getInt(1);
                            ret.add(cntSID);
                        } while (cursor.moveToNext());
                    }

                }
            }
        }
        db.close();

        String ret2 = "";
        for (int i=0; i<ret.size(); i++){
            if (i == 0)
                ret2 += "[ ";
            ret2 += ret.get(i).toString();
            if (i== ret.size()-1)
                ret2 += " ]";
            else ret2 += ", ";

        }
        return ret2;
    }

    public ArrayList<ContextContext> getAllUnsyncContexts (UserContext user){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM 'context' WHERE sID = '0' and userID = '" + user.getID() + "'";
        Cursor cursor = db.rawQuery(query, null);
        ArrayList<ContextContext> list = new ArrayList<ContextContext>();
        ContextContext context = new ContextContext();
        if (cursor.moveToFirst()) {
            do {
                context  = new ContextContext();
                context.setID(cursor.getInt(0));
                context.setsID(cursor.getInt(1));
                context.setUserID(cursor.getInt(2));
                context.setTitle(cursor.getString(3));
                context.setlChSec(cursor.getInt(4));
                list.add(context);
            } while (cursor.moveToNext());
        }
        db.close();
        return list;

    }

    public ContextContext getContextLastChangeFromSID(ContextContext context){
        SQLiteDatabase db = this.getWritableDatabase();
        // check if context exist

        ContextContext ccc = new ContextContext();
        String query = "SELECT * FROM 'context' WHERE sID = '" + context.getsID() + "'";
        Cursor cursor = db.rawQuery(query, null);


        if (cursor.moveToFirst()) {
            do {
                ccc.setID(cursor.getInt(0));
                ccc.setsID(cursor.getInt(1));
                ccc.setUserID(cursor.getInt(2));
                ccc.setTitle(cursor.getString(3));
                ccc.setlChSec(cursor.getInt(4));
            } while (cursor.moveToNext());
        }
        db.close();
        return ccc;
    }

    // ------------------------------------------------------------------------------------------
    // Work with Points
    public int addPoint(PointContext point){
        SQLiteDatabase db = this.getWritableDatabase();
        Calendar cal = Calendar.getInstance();
        long curDate = cal.getTime().getTime()/1000;
        //TODO add last change field
        String insertQuery = "INSERT INTO 'point' " +
                "('description', 'lat', 'lng', 'contextID', 'sID', 'lastChange') " +
                "VALUES " +
                "('" + point.getDescription() + "'," +
                "'" + point.getLat() + "', " +
                "'" + point.getLng() + "', " +
                "'" + point.getContextID() + "', " +
                "'" + point.getsID() + "'," +
                "'"+curDate+"')";
        db.execSQL(insertQuery);

        String query = "SELECT * FROM 'point' order by _id DESC limit 1";
        Cursor cursor = db.rawQuery(query, null);
        int id = 0;

        if (cursor.moveToFirst()) {
            do {
                id = cursor.getInt(0);
            } while (cursor.moveToNext());
        }

        db.close();
        return id;
    }

    public PointContext getPointByID(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM 'point' WHERE _id = '" + id + "'";
        Cursor cursor = db.rawQuery(query, null);

        PointContext point = new PointContext();

        if (cursor.moveToFirst()) {
            do {
                point  = new PointContext();
                point.setID(cursor.getInt(0));
                point.setContextID(cursor.getInt(1));
                point.setsID(cursor.getInt(2));
                point.setDescription(cursor.getString(3));
                point.setLat(cursor.getString(4));
                point.setLng(cursor.getString(5));
            } while (cursor.moveToNext());
        }
        db.close();
        return point;
    }

    public void changeTempPoint(PointContext point){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues newValues = new ContentValues();
        Calendar cal = Calendar.getInstance();
        long curDate = cal.getTime().getTime()/1000;
        // Назначьте значения для каждой строки.
        //newValues.put("contextID ", point.getContextID());
        newValues.put("sID", point.getsID());
        newValues.put("description", point.getDescription());
        newValues.put("lat", point.getLat());
        newValues.put("lng", point.getLng());
        newValues.put("contextID", point.getContextID());
        newValues.put("lastChange", curDate);


        String where = "_id =" + point.getID();
        // Обновите строку с указанным индексом, используя новые значения.
        db.update("point", newValues, where, null);

        db.close();
    }

    public void removePoint(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        PointContext point = getPointByID(id);
        String deleteQuery = "DELETE FROM 'point' WHERE _id = '"+id+"'";
        db.execSQL(deleteQuery);
        db.close();
    }

    public ArrayList<PointContext> getTempPoints(){
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<PointContext> points = new ArrayList<PointContext>();

        String selectQuery = "SELECT  * FROM point where contextID is null or contextID = 0";

        Cursor cursor = db.rawQuery(selectQuery, null);
        PointContext point;

        if (cursor.moveToFirst()) {
            do {
                point  = new PointContext();
                point.setID(cursor.getInt(0));
                point.setsID(cursor.getInt(2));
                point.setContextID(cursor.getInt(1));
                point.setDescription(cursor.getString(3));
                point.setLat(cursor.getString(4));
                point.setLng(cursor.getString(5));
                points.add(point);
            } while (cursor.moveToNext());
        }
        db.close();

        return points;
    }

    public ArrayList<PointContext> getAllPoints(){

        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<PointContext> points = new ArrayList<PointContext>();

        String selectQuery = "SELECT  * FROM point";

        Cursor cursor = db.rawQuery(selectQuery, null);
        PointContext point;

        if (cursor.moveToFirst()) {
            do {
                point  = new PointContext();
                point.setID(cursor.getInt(0));
                point.setsID(cursor.getInt(2));
                point.setContextID(cursor.getInt(1));
                point.setDescription(cursor.getString(3));
                point.setLat(cursor.getString(4));
                point.setLng(cursor.getString(5));
                points.add(point);
            } while (cursor.moveToNext());
        }
        db.close();

        return points;
    }

    public ArrayList<PointContext> getPointsByContextID(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<PointContext> points = new ArrayList<PointContext>();

        String selectQuery = "SELECT  * FROM point where contextID = '"+id+"'";

        Cursor cursor = db.rawQuery(selectQuery, null);
        PointContext point;

        if (cursor.moveToFirst()) {
            do {
                point  = new PointContext();
                point.setID(cursor.getInt(0));
                point.setsID(cursor.getInt(2));
                point.setContextID(cursor.getInt(1));
                point.setDescription(cursor.getString(3));
                point.setLat(cursor.getString(4));
                point.setLng(cursor.getString(5));
                points.add(point);
            } while (cursor.moveToNext());
        }
        db.close();

        return points;
    }

    public void updatePoints(ArrayList<PointContext> points, int cntID){
        SQLiteDatabase db = this.getWritableDatabase();
        Calendar cal = Calendar.getInstance();
        long curDate = cal.getTime().getTime()/1000;
        // для каждого в списке проставляем еще и context ID

        for (int i = 0; i < points.size(); i++){
            ContentValues newValues = new ContentValues();
            newValues.put("sID", points.get(i).getsID());
            newValues.put("contextID", cntID);
            newValues.put("description", points.get(i).getDescription());
            newValues.put("lat", points.get(i).getLat());
            newValues.put("lng", points.get(i).getLng());
            newValues.put("lastChange", curDate);

            String where = "_id =" + points.get(i).getID();

            db.update("point", newValues, where, null);

        }
        db.close();



    }

    public void updatePoint(PointContext point){
        SQLiteDatabase db = this.getWritableDatabase();
        Calendar cal = Calendar.getInstance();
        long curDate = cal.getTime().getTime()/1000;
        // для каждого в списке проставляем еще и context ID

        ContentValues newValues = new ContentValues();
        newValues.put("sID", point.getsID());
        newValues.put("contextID", point.getContextID());
        newValues.put("description", point.getDescription());
        newValues.put("lat", point.getLat());
            newValues.put("lng", point.getLng());
            newValues.put("lastChange", curDate);

            String where = "_id =" + point.getID();

            db.update("point", newValues, where, null);

        db.close();
    }

    public void removeAllContextPoints(int contextID){
        SQLiteDatabase db = this.getWritableDatabase();
        String deleteQuery = "DELETE FROM 'point' WHERE contextID = '"+contextID+"'";
        db.execSQL(deleteQuery);
        db.close();
    }



    // ------------------------------------------------------------------------------------------
    // Server
    public void saveTaskContextFromServer(int localTaskID, int serverContID){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor;
        int localCntID;

        String delQuery = "DELETE FROM 'task_context' WHERE taskID = '"+localTaskID+"'";
        db.execSQL(delQuery);
        // get local context_id
        String query = "SELECT * FROM 'context' WHERE sID = '" + serverContID + "'";
        cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                localCntID = cursor.getInt(0);
            } while (cursor.moveToNext());

            query = "INSERT into 'task_context' ('taskID', 'contextID') VALUES ('"+localTaskID+"', '"+localCntID+"')";
            db.execSQL(query);
        }
        db.close();
    }


    public String getTaskContextServerIDs(TaskContext task, UserContext user){
        SQLiteDatabase db = this.getWritableDatabase();
        String query;
        Cursor cursor;
        int userID = user.getID();
        ArrayList<Integer> ret = new ArrayList<Integer>();
        ArrayList<Integer> Sret = new ArrayList<Integer>();

        query = "SELECT * FROM 'task_context' where taskID = '" + task.getID() + "'";
        cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                int cntID = cursor.getInt(2);
                ret.add(cntID);
            } while (cursor.moveToNext());
        }

        for (int i = 0; i<ret.size(); i++){
            query = "SELECT * FROM 'context' WHERE _id = '"+ret.get(i)+"'";
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    int cntID = cursor.getInt(2);
                    Sret.add(cntID);
                } while (cursor.moveToNext());
            }
        }


        db.close();

        String ret2 = "";
        for (int i=0; i<Sret.size(); i++){
            if (i == 0)
                ret2 += "[ ";
            ret2 += Sret.get(i).toString();
            if (i== Sret.size()-1)
                ret2 += " ]";
            else ret2 += ", ";

        }
        return ret2;
    }
}

