package com.example.maria.testnavbar.Provider;

import java.util.Date;

public class UserContext {

    private int ID;
    private int sID;
    private String login;
    private String password;
    private String email;
    private int active;
    private Date lastChange;

    public UserContext() {}
    public UserContext(int ID, String LOGIN, String PASS, String EMAIL) {
        this.ID = ID;
        this.login = LOGIN;
        this.password = PASS;
        this.email = EMAIL;
    }

    public UserContext(int sID, String LOGIN, String PASS, String EMAIL, int act) {
        this.sID = sID;
        this.login = LOGIN;
        this.password = PASS;
        this.email = EMAIL;
        this.active = act;
    }

    public int getID() {return this.ID;}
    public int getsID() {return  this.sID;}
    public String getLogin() {return this.login;}
    public String getPassword() {return this.password;}
    public String getEmail() {return this.email;}
    public int getActive() {return this.active;}
    public Date getLastChange() {return this.lastChange;}

    public void setID(int ID) {this.ID = ID;}
    public void setsID(int SID) {this.sID = SID;}
    public void setLogin(String LOGIN) {this.login = LOGIN;}
    public void setPassword(String PASSWORD) {this.password = PASSWORD;}
    public void setEmail(String EMAIL) {this.email = EMAIL;}
    public void setActive(int FLAG) {this.active = FLAG;}
    public void setLastChange(Date LAST) {this.lastChange = LAST;}
}
