package com.example.maria.testnavbar.Activities;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.maria.testnavbar.Provider.DBWorker;
import com.example.maria.testnavbar.Provider.UserContext;
import com.example.maria.testnavbar.R;
import com.example.maria.testnavbar.RestAPI.WebWorker;

public class RegisterActivity extends ActionBarActivity {

    EditText regEmail;
    EditText regLogin;
    EditText regPass;
    EditText regPassConf;
    Button regBtn;
    TextView regAuthLink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        regEmail = (EditText) findViewById(R.id.regEmail);
        regLogin = (EditText) findViewById(R.id.regLogin);
        regPass = (EditText) findViewById(R.id.regPass);
        regPassConf = (EditText) findViewById(R.id.regPassConf);
        regBtn = (Button) findViewById(R.id.regBtn);
        regAuthLink = (TextView) findViewById(R.id.regAuthLink);

        regBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                registration();
            }
        });

        regAuthLink.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this, AuthActivity.class);
                RegisterActivity.this.startActivity(intent);
            }
        });
    }

    public void registration(){
        String email = regEmail.getText().toString();
        String login = regLogin.getText().toString();
        String pass = regPass.getText().toString();
        String confPass = regPassConf.getText().toString();


        if (email.length()>0 && login.length()>0 && pass.length()>0 && pass.equals(confPass)){


       /* WebWorker www = new WebWorker(this);
        if (www.hasInternetConnection()) {
            UserContext newUser = www.registerUser(email, login, pass);
            if (newUser.getEmail() != null) {
                DBWorker db = new DBWorker(this);
                db.registerUser(newUser);
                Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                startActivity(intent);
                this.finish();
            }
        }
            else {
            Toast toast2 = Toast.makeText(getApplicationContext(), "Отсутствует подключение к интернету, повторите попытку позже", Toast.LENGTH_LONG);
            toast2.show();
        }
        }
        else {
            Toast toast2 = Toast.makeText(getApplicationContext(), "Ошибка в заполнении полей!", Toast.LENGTH_LONG);
            toast2.show();
        }
*/
            UserContext newUser = new UserContext();
            newUser.setEmail(email);
            newUser.setPassword(pass);
            newUser.setLogin(login);

            DBWorker db = new DBWorker(this);
            db.registerUser(newUser);
            Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
            startActivity(intent);
            this.finish();}

    }
}
