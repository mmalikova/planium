package com.example.maria.testnavbar.Fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.maria.testnavbar.Adapters.LocationAdapter;
import com.example.maria.testnavbar.Provider.ContextContext;
import com.example.maria.testnavbar.Provider.DBWorker;
import com.example.maria.testnavbar.Provider.UserContext;
import com.example.maria.testnavbar.R;
import com.example.maria.testnavbar.RestAPI.WebWorker;

import java.util.ArrayList;


public class LocationList extends Fragment {
    private LocationAdapter adapter;

    private ListView lv;
    private DBWorker db;
    private ArrayList<ContextContext> points;
    UserContext activeUser;


    public LocationList(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.list_location, container,
                false);
        db = new DBWorker(getActivity());
        // получаем активного пользователя
        activeUser = db.checkUser();

        // получаем список всех точек
        points = db.getAllContexts(activeUser.getID());

        lv = (ListView)view.findViewById(R.id.listViewLocations);
        //выводим в листе
        adapter = new LocationAdapter(getActivity(), R.id.listViewLocations, points);
        lv.setAdapter(adapter);
        // Обработка события на клик по элементу списка
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                // переходим на фрагмент со списком привязанных заметок
                Location yfc = new Location();
                Bundle bundle = new Bundle();
                bundle.putInt("contextID", points.get(position).getID());
                yfc.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_frame,  yfc).addToBackStack(null).commit();
            }
        });

        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,int position, long id) {
                ContextMenu(position);
                return true;
            }
        });

        getActivity().setTitle("Контексты");

        return view;
    }


    public void ContextMenu(int position){
        final int pos = position;
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final String[] actions ={"Редактировать", "Удалить"};
        builder.setTitle("Возможные действия");

        builder.setItems(actions, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        Location yfc = new Location();
                        Bundle bundle = new Bundle();
                        bundle.putInt("contextID", points.get(pos).getID());
                        yfc.setArguments(bundle);
                        FragmentManager fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.content_frame,  yfc).addToBackStack(null).commit();
                        break;
                    case 1:

                        // todo remove context and all its point from server
                        WebWorker www = new WebWorker(getActivity());
                        if (points.get(pos).getsID()>0){
                            www.deleteContext(points.get(pos), activeUser);
                        }

                        db.removeContext(points.get(pos).getID());
                        points.remove(pos);
                        adapter.notifyDataSetChanged();
                        getActivity().recreate();
                        break;
                    default: break;
                }

            }
        });
        builder.setCancelable(true);
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.location, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_new_location:

                //переходим на фрагмент добавления положения
                Location yfc = new Location();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_frame,  yfc).commit();
                return true;

            default:
                // Not one of ours. Perform default menu processing
                return super.onOptionsItemSelected(item);
        }
    }
}