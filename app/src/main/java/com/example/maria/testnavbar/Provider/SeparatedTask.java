package com.example.maria.testnavbar.Provider;


public class SeparatedTask {
    private int ID;
    private String title;
    private int isChecked;
    private int needSeparator;
    private String separatorTitle;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(int isChecked) {
        this.isChecked = isChecked;
    }

    public int getNeedSeparator() {
        return needSeparator;
    }

    public void setNeedSeparator(int needSeparator) {
        this.needSeparator = needSeparator;
    }

    public String getSeparatorTitle() {
        return separatorTitle;
    }

    public void setSeparatorTitle(String separatorTitle) {
        this.separatorTitle = separatorTitle;
    }



}
