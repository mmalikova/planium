package com.example.maria.testnavbar.Provider;

import java.util.Date;

public class KeywordContext {

    private int ID;
    private int sID;
    private int contextID;
    private String word;
    private Date lastChange;

    public int getID() { return ID;}
    public int getsID() { return sID;}
    public int getContextID() { return contextID;}
    public String getWord() { return word;}
    public Date getLastChange() {return lastChange;}

    public void setID(int ID) { this.ID = ID;}
    public void setsID(int sID) { this.sID = sID;}
    public void setContextID(int contextID) { this.contextID = contextID;}
    public void setWord(String word) {this.word = word;}
    public void setLastChange(Date lastChange) {this.lastChange = lastChange;}


}
