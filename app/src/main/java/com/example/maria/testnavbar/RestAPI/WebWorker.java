package com.example.maria.testnavbar.RestAPI;


import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.maria.testnavbar.Provider.ContextContext;
import com.example.maria.testnavbar.Provider.DBWorker;
import com.example.maria.testnavbar.Provider.PointContext;
import com.example.maria.testnavbar.Provider.SubtaskContext;
import com.example.maria.testnavbar.Provider.TaskContext;
import com.example.maria.testnavbar.Provider.UserContext;
import com.example.maria.testnavbar.Services.ContextNotify;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ExecutionException;

public class WebWorker {

    Context cnt;
    public static final String APP_PREFERENCES = "mysettings";
    public static final String APP_PREFERENCES_COUNTER = "token";
    private SharedPreferences mSettings;
    private DBWorker db;


    public WebWorker(Context context) {
        this.cnt = context;
        mSettings = cnt.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        db = new DBWorker(cnt);

    }

    public UserContext getToken(String pass, String email) {
        try {

            RequestContext request = new RequestContext();

            request.setMethod("GET");
            request.setLogin(email);
            request.setPass(pass);
            request.setUrl("http://irinn.us/api/v1.0/token/");

            AsyncRequest async = new AsyncRequest(cnt);
            async.execute(request);
            ResponseContext response = async.get();

            switch (response.getResponseCode()) {
                case -1:
                    //Toast.makeText(cnt, "Ошибка с запросом", Toast.LENGTH_LONG).show();
                    break;
                case 200:
                    // все хорошо
                    try {
                        if (response.getResponse().get("token").toString().length() > 0) {
                            // Запоминаем данные
                            SharedPreferences.Editor editor = mSettings.edit();
                            editor.putString(APP_PREFERENCES_COUNTER, response.getResponse().get("token").toString());
                            editor.apply();

                            return new UserContext(Integer.parseInt(response.getResponse().get("id").toString()), response.getResponse().get("name").toString(), pass, email, 1);

                        }
                    } catch (JSONException e) {
                        //Toast toast2 = Toast.makeText(cnt, "Ошибка авторизации", Toast.LENGTH_LONG);
                        //toast2.show();
                        return null;
                    }
                    break;
                case 401:
                    break;
                default:
                    //Toast.makeText(cnt, "Вообще не понятно, что вернулось с сервера", Toast.LENGTH_LONG).show();
                    return null;
            }

        } catch (InterruptedException e) {
            Log.d("EX: ", e.toString());
            return null;
        } catch (ExecutionException e) {
            Log.d("EX: ", e.toString());
            return null;
        }
        return null;
    }

    public UserContext registerUser(String email, String login, String pass) {
        UserContext newUser = new UserContext();
        try {
            JSONObject json = new JSONObject();
            json.put("name", login);
            json.put("email", email);
            json.put("password", pass);

            RequestContext request = new RequestContext();

            request.setMethod("POST");
            request.setJsonData(json);
            request.setUrl("http://irinn.us/api/v1.0/user/");

            AsyncRequest async = new AsyncRequest(cnt);
            async.execute(request);
            ResponseContext response = async.get();

            switch (response.getResponseCode()) {
                case -1:
                    //Toast.makeText(cnt, "Ошибка с запросом", Toast.LENGTH_LONG).show();
                    break;
                case 201:
                    newUser = getToken(pass, email);
                    break;
                case 405:
                   // Toast.makeText(cnt, "Пользователь с таким email уже существует", Toast.LENGTH_LONG).show();
                    break;
                case 400:
                    //Toast.makeText(cnt, "Вообще ничего не отправилось(", Toast.LENGTH_LONG).show();
                    break;
                default:
                    //Toast.makeText(cnt, "Вообще не понятно, что вернулось с сервера", Toast.LENGTH_LONG).show();
                    break;
            }

        } catch (JSONException e) {
            Log.d("Error JSONException: ", e.toString());
        } catch (InterruptedException e) {
            Log.d("Error InterruptedExc: ", e.toString());
        } catch (ExecutionException e) {
            Log.d("Error ExecutionExc: ", e.toString());
        }

        return newUser;
    }

    public ContextContext addContext(String title, UserContext user) {
        ContextContext newContext = new ContextContext();

        // todo try to get token
        String mCounter = mSettings.getString(APP_PREFERENCES_COUNTER, "");
        /*if (mCounter.length() == 0) {
            UserContext newUser = getToken(user.getPassword(), user.getEmail());
            mCounter = mSettings.getString(APP_PREFERENCES_COUNTER, "");
        }*/

        try {
            Calendar cal = Calendar.getInstance();
            Date curDate = cal.getTime();

            RequestContext request = new RequestContext();
            JSONObject json = new JSONObject();
            json.put("last_change", curDate.getTime() / 1000);
            json.put("title", title);

            request.setMethod("POST");
            request.setUrl("http://irinn.us/api/v1.0/contexts/");
            request.setJsonData(json);
            request.setLogin(user.getEmail());
            request.setPass(user.getPassword());

            HTTPClientRequest async = new HTTPClientRequest(cnt);
            //AsyncRequest async = new AsyncRequest(cnt);
            async.execute(request);
            ResponseContext response = async.get();

            if (response.getResponse() != null) {
                switch (response.getResponseCode()) {
                    case 201:
                        newContext.setsID(Integer.parseInt(response.getResponse().get("id").toString()));
                        newContext.setTitle(response.getResponse().get("title").toString());
                        newContext.setlChSec(response.getResponse().getInt("last_change"));
                        newContext.setUserID(user.getID());
                        break;
                    case 400:
                       // Toast.makeText(cnt, "отсутствует обязательный параметр", Toast.LENGTH_LONG).show();
                        break;
                    case 500:
                        //Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                        break;
                    default:
                       // Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                        break;
                }
            }
        } catch (JSONException ex) {
            Log.e("ERROR", ex.toString());
        } catch (ExecutionException ex) {
            Log.e("ERROR", ex.toString());
        } catch (InterruptedException ex) {
            Log.e("ERROR", ex.toString());
        }

        return newContext;
    }

    public PointContext addPoint(PointContext point, ContextContext kontext, UserContext user) {


        // todo try to get token
           /* String mCounter = mSettings.getString(APP_PREFERENCES_COUNTER, "");
            if (mCounter.length() == 0){
                UserContext newUser = getToken(user.getPassword(), user.getEmail());
                mCounter = mSettings.getString(APP_PREFERENCES_COUNTER, "");
            }*/


        try {
            Calendar cal = Calendar.getInstance();
            Date curDate = cal.getTime();

            RequestContext request = new RequestContext();
            JSONObject json = new JSONObject();
            json.put("context", kontext.getsID());
            json.put("description", point.getDescription());
            json.put("last_change", curDate.getTime() / 1000);
            json.put("lat", point.getLat());
            json.put("lng", point.getLng());


            request.setMethod("POST");
            request.setUrl("http://irinn.us/api/v1.0/points/");
            request.setJsonData(json);
            request.setLogin(user.getEmail());
            request.setPass(user.getPassword());

            HTTPClientRequest async = new HTTPClientRequest(cnt);
            async.execute(request);
            ResponseContext response = async.get();

            switch (response.getResponseCode()) {
                case 201:
                    point.setsID(Integer.parseInt(response.getResponse().get("id").toString()));
                    break;
                case 400:
                    //Toast.makeText(cnt, "отсутствует обязательный параметр", Toast.LENGTH_LONG).show();
                    break;
                case 500:
                    //Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                    break;
                default:
                   // Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                    break;
            }
        } catch (JSONException ex) {
            Log.e("ERROR", ex.toString());
        } catch (ExecutionException ex) {
            Log.e("ERROR", ex.toString());
        } catch (InterruptedException ex) {
            Log.e("ERROR", ex.toString());
        }

        return point;
    }

    public boolean updateContext(ContextContext kontext, UserContext user) {
        boolean ret = false;
        // todo try to get token
           /* String mCounter = mSettings.getString(APP_PREFERENCES_COUNTER, "");
            if (mCounter.length() == 0){
                UserContext newUser = getToken(user.getPassword(), user.getEmail());
                mCounter = mSettings.getString(APP_PREFERENCES_COUNTER, "");
            }*/

        try {
            Calendar cal = Calendar.getInstance();
            Date curDate = cal.getTime();

            RequestContext request = new RequestContext();
            JSONObject json = new JSONObject();
            json.put("title", kontext.getTitle());
            json.put("last_change", curDate.getTime() / 1000);
            request.setMethod("PUT");
            request.setUrl("http://irinn.us/api/v1.0/contexts/" + kontext.getsID());
            request.setJsonData(json);
            request.setLogin(user.getEmail());
            request.setPass(user.getPassword());

            HTTPClientRequest async = new HTTPClientRequest(cnt);
            async.execute(request);
            ResponseContext response = async.get();

            switch (response.getResponseCode()) {
                case 200:
                    ret = true;
                    //point.setsID(Integer.parseInt(response.getResponse().get("id").toString()));
                    break;
                case 400:
                   // Toast.makeText(cnt, "отсутствует обязательный параметр", Toast.LENGTH_LONG).show();
                    break;
                case 500:
                   // Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                    break;
                default:
                   // Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                    break;
            }
        } catch (JSONException ex) {
            Log.e("ERROR", ex.toString());
        } catch (ExecutionException ex) {
            Log.e("ERROR", ex.toString());
        } catch (InterruptedException ex) {
            Log.e("ERROR", ex.toString());
        }

        return ret;
    }

    public boolean deleteContext(ContextContext kontext, UserContext user) {
        boolean ret = false;
        // todo try to get token
           /* String mCounter = mSettings.getString(APP_PREFERENCES_COUNTER, "");
            if (mCounter.length() == 0){
                UserContext newUser = getToken(user.getPassword(), user.getEmail());
                mCounter = mSettings.getString(APP_PREFERENCES_COUNTER, "");
            }*/

        try {
            Calendar cal = Calendar.getInstance();
            Date curDate = cal.getTime();

            RequestContext request = new RequestContext();
            JSONObject json = new JSONObject();
            // todo set last_change from db
            json.put("last_change", curDate.getTime() / 1000);
            request.setMethod("DELETE");
            request.setUrl("http://irinn.us/api/v1.0/contexts/" + kontext.getsID());
            request.setJsonData(json);
            request.setLogin(user.getEmail());
            request.setPass(user.getPassword());

            HTTPClientRequest async = new HTTPClientRequest(cnt);
            async.execute(request);
            ResponseContext response = async.get();

            switch (response.getResponseCode()) {
                case 204:
                    ret = true;
                    break;
                case 400:
                   // Toast.makeText(cnt, "отсутствует обязательный параметр", Toast.LENGTH_LONG).show();
                    break;
                case 500:
                   // Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                    break;
                default:
                   // Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                    break;
            }
        } catch (JSONException ex) {
            Log.e("ERROR", ex.toString());
        } catch (ExecutionException ex) {
            Log.e("ERROR", ex.toString());
        } catch (InterruptedException ex) {
            Log.e("ERROR", ex.toString());
        }

        return ret;
    }

    public boolean deletePoint(PointContext point, UserContext user) {
        boolean ret = false;
        // todo try to get token
           /* String mCounter = mSettings.getString(APP_PREFERENCES_COUNTER, "");
            if (mCounter.length() == 0){
                UserContext newUser = getToken(user.getPassword(), user.getEmail());
                mCounter = mSettings.getString(APP_PREFERENCES_COUNTER, "");
            }*/

        try {
            Calendar cal = Calendar.getInstance();
            Date curDate = cal.getTime();

            RequestContext request = new RequestContext();
            JSONObject json = new JSONObject();
            // todo set last_change from db
            json.put("last_change", curDate.getTime() / 1000);
            request.setMethod("DELETE");
            request.setUrl("http://irinn.us/api/v1.0/points/" + point.getsID());
            request.setJsonData(json);
            request.setLogin(user.getEmail());
            request.setPass(user.getPassword());

            HTTPClientRequest async = new HTTPClientRequest(cnt);
            async.execute(request);
            ResponseContext response = async.get();

            switch (response.getResponseCode()) {
                case 204:
                    ret = true;
                    break;
                case 400:
                   // Toast.makeText(cnt, "отсутствует обязательный параметр", Toast.LENGTH_LONG).show();
                    break;
                case 500:
                   // Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                    break;
                default:
                   // Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                    break;
            }
        } catch (JSONException ex) {
            Log.e("ERROR", ex.toString());
        } catch (ExecutionException ex) {
            Log.e("ERROR", ex.toString());
        } catch (InterruptedException ex) {
            Log.e("ERROR", ex.toString());
        }

        return ret;
    }

    public boolean updatePoint(PointContext point, UserContext user, int sID) {
        boolean ret = false;
        // todo try to get token
           /* String mCounter = mSettings.getString(APP_PREFERENCES_COUNTER, "");
            if (mCounter.length() == 0){
                UserContext newUser = getToken(user.getPassword(), user.getEmail());
                mCounter = mSettings.getString(APP_PREFERENCES_COUNTER, "");
            }*/

        try {
            Calendar cal = Calendar.getInstance();
            Date curDate = cal.getTime();

            RequestContext request = new RequestContext();
            JSONObject json = new JSONObject();
            //json.put("title", point.getTitle());
            json.put("last_change", curDate.getTime() / 1000);
            json.put("context", sID);
            json.put("description", point.getDescription());
            json.put("lat", point.getLat());
            json.put("lng", point.getLng());
            request.setMethod("PUT");
            request.setUrl("http://irinn.us/api/v1.0/points/" + point.getsID());
            request.setJsonData(json);
            request.setLogin(user.getEmail());
            request.setPass(user.getPassword());

            HTTPClientRequest async = new HTTPClientRequest(cnt);
            async.execute(request);
            ResponseContext response = async.get();

            switch (response.getResponseCode()) {
                case 200:
                    ret = true;
                    //point.setsID(Integer.parseInt(response.getResponse().get("id").toString()));
                    break;
                case 400:
                    //Toast.makeText(cnt, "отсутствует обязательный параметр", Toast.LENGTH_LONG).show();
                    break;
                case 500:
                   // Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                    break;
                default:
                   // Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                    break;
            }
        } catch (JSONException ex) {
            Log.e("ERROR", ex.toString());
        } catch (ExecutionException ex) {
            Log.e("ERROR", ex.toString());
        } catch (InterruptedException ex) {
            Log.e("ERROR", ex.toString());
        }

        return ret;
    }

    public int addTask(TaskContext task, UserContext user, String scID) {
        int servId = -1;

        try {
            Calendar cal = Calendar.getInstance();
            Date curDate = cal.getTime();

            RequestContext request = new RequestContext();
            JSONObject json = new JSONObject();
            json.put("checked", task.getChecked());
            if (task.getDueDate() != null) {
                json.put("due_date", task.getDueDate().getTime() / 1000);
            } else {
                json.put("due_date", 0);
            }
            json.put("last_change", curDate.getTime() / 1000);
            json.put("notice", task.getNotice());
            json.put("location_notice", task.getLocationNotice());
            json.put("repeat_id", task.getRepeatID());
            json.put("text", task.getText());
            json.put("contexts", scID);

            request.setMethod("POST");
            request.setUrl("http://irinn.us/api/v1.0/tasks/");
            request.setJsonData(json);
            request.setLogin(user.getEmail());
            request.setPass(user.getPassword());

            HTTPClientRequest async = new HTTPClientRequest(cnt);
            async.execute(request);
            ResponseContext response = async.get();

            switch (response.getResponseCode()) {
                case 201:
                    servId = Integer.parseInt(response.getResponse().get("id").toString());
                    break;
                case 400:
                    //Toast.makeText(cnt, "отсутствует обязательный параметр", Toast.LENGTH_LONG).show();
                    break;
                case 500:
                    //Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                    break;
                default:
                    //Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                    break;
            }
        } catch (JSONException ex) {
            Log.e("ERROR", ex.toString());
        } catch (ExecutionException ex) {
            Log.e("ERROR", ex.toString());
        } catch (InterruptedException ex) {
            Log.e("ERROR", ex.toString());
        }

        return servId;
    }

    public int addSubTask(SubtaskContext sub, UserContext user, int servTaskID) {
        int servId = -1;

        try {
            Calendar cal = Calendar.getInstance();
            Date curDate = cal.getTime();

            RequestContext request = new RequestContext();
            JSONObject json = new JSONObject();
            json.put("task", servTaskID);
            json.put("text", sub.getText());
            json.put("last_change", curDate.getTime() / 1000);

            request.setMethod("POST");
            request.setUrl("http://irinn.us/api/v1.0/subtasks/");
            request.setJsonData(json);
            request.setLogin(user.getEmail());
            request.setPass(user.getPassword());

            HTTPClientRequest async = new HTTPClientRequest(cnt);
            async.execute(request);
            ResponseContext response = async.get();

            switch (response.getResponseCode()) {
                case 201:
                    servId = Integer.parseInt(response.getResponse().get("id").toString());
                    break;
                case 400:
                    //Toast.makeText(cnt, "отсутствует обязательный параметр", Toast.LENGTH_LONG).show();
                    break;
                case 500:
                    //Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                    break;
                default:
                    //Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                    break;
            }
        } catch (JSONException ex) {
            Log.e("ERROR", ex.toString());
        } catch (ExecutionException ex) {
            Log.e("ERROR", ex.toString());
        } catch (InterruptedException ex) {
            Log.e("ERROR", ex.toString());
        }

        return servId;
    }

    public boolean deleteTask(TaskContext task, UserContext user) {
        boolean ret = false;
        try {
            Calendar cal = Calendar.getInstance();
            Date curDate = cal.getTime();

            RequestContext request = new RequestContext();
            JSONObject json = new JSONObject();
            // todo set last_change from db
            json.put("last_change", curDate.getTime() / 1000);
            request.setMethod("DELETE");
            request.setUrl("http://irinn.us/api/v1.0/tasks/" + task.getsID());
            request.setJsonData(json);
            request.setLogin(user.getEmail());
            request.setPass(user.getPassword());

            HTTPClientRequest async = new HTTPClientRequest(cnt);
            async.execute(request);
            ResponseContext response = async.get();

            switch (response.getResponseCode()) {
                case 204:
                    ret = true;
                    break;
                case 400:
                    //Toast.makeText(cnt, "отсутствует обязательный параметр", Toast.LENGTH_LONG).show();
                    break;
                case 500:
                    //Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                    break;
                default:
                    //Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                    break;
            }
        } catch (JSONException ex) {
            Log.e("ERROR", ex.toString());
        } catch (ExecutionException ex) {
            Log.e("ERROR", ex.toString());
        } catch (InterruptedException ex) {
            Log.e("ERROR", ex.toString());
        }

        return ret;
    }

    public boolean deleteSubTask(SubtaskContext sub, UserContext user) {
        boolean ret = false;
        try {
            Calendar cal = Calendar.getInstance();
            Date curDate = cal.getTime();

            RequestContext request = new RequestContext();
            JSONObject json = new JSONObject();
            // todo set last_change from db
            json.put("last_change", curDate.getTime() / 1000);
            request.setMethod("DELETE");
            request.setUrl("http://irinn.us/api/v1.0/subtasks/" + sub.getsID());
            request.setJsonData(json);
            request.setLogin(user.getEmail());
            request.setPass(user.getPassword());

            HTTPClientRequest async = new HTTPClientRequest(cnt);
            async.execute(request);
            ResponseContext response = async.get();

            switch (response.getResponseCode()) {
                case 204:
                    ret = true;
                    break;
                case 400:
                    //Toast.makeText(cnt, "отсутствует обязательный параметр", Toast.LENGTH_LONG).show();
                    break;
                case 500:
                    //Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                    break;
                default:
                    //Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                    break;
            }
        } catch (JSONException ex) {
            Log.e("ERROR", ex.toString());
        } catch (ExecutionException ex) {
            Log.e("ERROR", ex.toString());
        } catch (InterruptedException ex) {
            Log.e("ERROR", ex.toString());
        }

        return ret;
    }

    public boolean updateTask(TaskContext task, UserContext user, String scID) {
        boolean ret = false;
        // todo try to get token
           /* String mCounter = mSettings.getString(APP_PREFERENCES_COUNTER, "");
            if (mCounter.length() == 0){
                UserContext newUser = getToken(user.getPassword(), user.getEmail());
                mCounter = mSettings.getString(APP_PREFERENCES_COUNTER, "");
            }*/

        try {
            Calendar cal = Calendar.getInstance();
            Date curDate = cal.getTime();

            RequestContext request = new RequestContext();
            JSONObject json = new JSONObject();
            json.put("checked", task.getChecked());
            if (task.getDueDate() != null) {
                json.put("due_date", task.getDueDate().getTime() / 1000);
            } else {
                json.put("due_date", 0);
            }
            json.put("last_change", curDate.getTime() / 1000);
            json.put("notice", task.getNotice());
            json.put("location_notice", task.getLocationNotice());
            json.put("repeat_id", task.getRepeatID());
            json.put("text", task.getText());
            json.put("contexts", scID);
            request.setMethod("PUT");
            request.setUrl("http://irinn.us/api/v1.0/tasks/" + task.getsID());
            request.setJsonData(json);
            request.setLogin(user.getEmail());
            request.setPass(user.getPassword());

            HTTPClientRequest async = new HTTPClientRequest(cnt);
            async.execute(request);
            ResponseContext response = async.get();

            switch (response.getResponseCode()) {
                case 200:
                    ret = true;
                    //point.setsID(Integer.parseInt(response.getResponse().get("id").toString()));
                    break;
                case 400:
                    //Toast.makeText(cnt, "отсутствует обязательный параметр", Toast.LENGTH_LONG).show();
                    break;
                case 500:
                    //Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                    break;
                default: //Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                    break;
            }
        } catch (JSONException ex) {
            Log.e("ERROR", ex.toString());
        } catch (ExecutionException ex) {
            Log.e("ERROR", ex.toString());
        } catch (InterruptedException ex) {
            Log.e("ERROR", ex.toString());
        }

        return ret;
    }

    public void makeFullSync(UserContext user) {

        DBWorker db = new DBWorker(cnt);

        //if (hasInternetConnection()) {
            // get all context from db
            try {
                RequestContext request = new RequestContext();
                request.setLogin(user.getEmail());
                request.setPass(user.getPassword());
                request.setMethod("GET");
                request.setUrl("http://irinn.us/api/v1.0/contexts/");

                HTTPClientRequest async = new HTTPClientRequest(cnt);
                async.execute(request);
                ResponseContext response = async.get();

                switch (response.getResponseCode()) {
                    case 200:
                        // read data from json
                        JSONArray contexts = response.getResponse().getJSONArray("contexts");
                        for (int i = 0; i < contexts.length(); i++) {
                            JSONObject context = contexts.getJSONObject(i);
                            JSONArray pointsIDs = context.getJSONArray("points");

                            ContextContext contextDB = new ContextContext();
                            contextDB.setUserID(user.getID());
                            contextDB.setTitle(context.getString("title"));
                            contextDB.setsID(context.getInt("id"));
                            contextDB.setlChSec(context.getInt("last_change"));

                            int newCNTID = db.addContext(contextDB);

                            for (int k = 0; k < pointsIDs.length(); k++) {

                                int id = pointsIDs.getInt(k);
                                getPointByServerID(user, id, newCNTID);

                            }
                        }
                        break;
                    case 400:
                        //Toast.makeText(cnt, "отсутствует обязательный параметр", Toast.LENGTH_LONG).show();
                        break;
                    case 500:
                       // Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                        break;
                    default:
                        //Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                        break;
                }

                // get all tasks
                request = new RequestContext();
                request.setLogin(user.getEmail());
                request.setPass(user.getPassword());
                request.setMethod("GET");
                request.setUrl("http://irinn.us/api/v1.0/tasks/");

                async = new HTTPClientRequest(cnt);
                async.execute(request);
                response = async.get();

                switch (response.getResponseCode()) {
                    case 200:
                        // read data from json
                        JSONArray tasks = response.getResponse().getJSONArray("tasks");
                        for (int i = 0; i < tasks.length(); i++) {
                            JSONObject task = tasks.getJSONObject(i);

                            String t = task.getString("subtask");
                            JSONArray subIDs = null;
                            if (t != "null")
                                subIDs = task.getJSONArray("subtask");

                            t = task.getString("contexts");
                            JSONArray t_cnt = null;
                            if (t != "null")
                                t_cnt = task.getJSONArray("contexts");

                            TaskContext taskDB = new TaskContext();

                            taskDB.setUserID(user.getID());
                            taskDB.setsID(task.getInt("id"));
                            taskDB.setLastChange(new Date(task.getInt("last_change") * 1000));
                            taskDB.setChecked(task.getInt("checked"));
                            taskDB.setDueDate(new Date((long) task.getInt("due_date") * 1000));
                            taskDB.setNotice(new Date((long) task.getInt("notice") * 1000));
                            taskDB.setRepeatID(task.getInt("repeat_id"));
                            taskDB.setText(task.getString("text"));
                            taskDB.setLocationNotice(task.getInt("location_notice"));

                            int newTaskID = db.newTask(taskDB);
                            // add all subtasks

                            if (subIDs != null) {
                                for (int k = 0; k < subIDs.length(); k++) {
                                    int id = subIDs.getInt(k);
                                    getSubTaskByServerID(user, id, newTaskID);

                                }
                            }

                            if (t_cnt != null) {
                                // add t_cnt
                                for (int l = 0; l < t_cnt.length(); l++) {
                                    int id = t_cnt.getInt(l);
                                    db.saveTaskContextFromServer(newTaskID, id);
                                }
                            }
                        }
                        break;
                    case 400:
                        //Toast.makeText(cnt, "отсутствует обязательный параметр", Toast.LENGTH_LONG).show();
                        break;
                    case 500:
                        //Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                        break;
                    default:
                        //Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                        break;
                }

            } catch (JSONException ex) {
                Log.e("ERROR", ex.toString());
            } catch (ExecutionException ex) {
                Log.e("ERROR", ex.toString());
            } catch (InterruptedException ex) {
                Log.e("ERROR", ex.toString());
            }
            String now = Long.toString(Calendar.getInstance().getTimeInMillis() / 1000);
            int nnow = Integer.parseInt(now);
            SharedPreferences.Editor editor = mSettings.edit();
            editor.putInt("sync", nnow);
            editor.apply();
      //  }
    }

    public void getPointByServerID(UserContext user, int servID, int LocalCntID) {

        DBWorker db = new DBWorker(cnt);
        try {
            RequestContext request = new RequestContext();
            request.setLogin(user.getEmail());
            request.setPass(user.getPassword());
            request.setMethod("GET");
            request.setUrl("http://irinn.us/api/v1.0/points/" + servID);

            HTTPClientRequest async = new HTTPClientRequest(cnt);
            async.execute(request);
            ResponseContext response = async.get();
            switch (response.getResponseCode()) {
                case 200:
                    PointContext point = new PointContext();
                    point.setDescription(response.getResponse().getString("description"));
                    point.setsID(response.getResponse().getInt("id"));
                    point.setContextID(LocalCntID);
                    point.setLat(response.getResponse().getString("lat"));
                    point.setLng(response.getResponse().getString("lng"));
                    point.setLastChange(new Date(response.getResponse().getInt("last_change") * 1000));

                    db.addPoint(point);
                    break;
                case 400:
                    //Toast.makeText(cnt, "отсутствует обязательный параметр", Toast.LENGTH_LONG).show();
                    break;
                case 500:
                    //Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                    break;
                default:
                    //Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                    break;
            }
        } catch (JSONException ex) {
            Log.e("ERROR", ex.toString());
        } catch (ExecutionException ex) {
            Log.e("ERROR", ex.toString());
        } catch (InterruptedException ex) {
            Log.e("ERROR", ex.toString());
        }
    }

    public void getSubTaskByServerID(UserContext user, int servID, int LocalTaskID) {

        DBWorker db = new DBWorker(cnt);
        try {
            RequestContext request = new RequestContext();
            request.setLogin(user.getEmail());
            request.setPass(user.getPassword());
            request.setMethod("GET");
            request.setUrl("http://irinn.us/api/v1.0/subtasks/" + servID);

            HTTPClientRequest async = new HTTPClientRequest(cnt);
            async.execute(request);
            ResponseContext response = async.get();
            switch (response.getResponseCode()) {
                case 200:
                    SubtaskContext sub = new SubtaskContext();
                    sub.setText(response.getResponse().getString("text"));
                    sub.setsID(response.getResponse().getInt("id"));
                    sub.setTaskID(LocalTaskID);
                    sub.setLastChange(new Date(response.getResponse().getInt("last_change") * 1000));
                    db.addSubTask(sub);
                    break;
                case 400:
                    //Toast.makeText(cnt, "отсутствует обязательный параметр", Toast.LENGTH_LONG).show();
                    break;
                case 500:
                    //Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                    break;
                default:
                    //Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                    break;
            }
        } catch (JSONException ex) {
            Log.e("ERROR", ex.toString());
        } catch (ExecutionException ex) {
            Log.e("ERROR", ex.toString());
        } catch (InterruptedException ex) {
            Log.e("ERROR", ex.toString());
        }
    }


    public void makeSync(UserContext user) {

        if (hasInternetConnection()) {
            String APP_PREFERENCES_COUNTER = "sync";
            SharedPreferences mSettings = cnt.getSharedPreferences("mysettings", Context.MODE_PRIVATE);
            int sync = mSettings.getInt("sync", 0);
            if (sync == 0) {
                makeFullSync(user);
            } else {

                syncContexts(user, sync);
                syncTasks(user, sync);

                String now = Long.toString(Calendar.getInstance().getTimeInMillis() / 1000);
                int nnow = Integer.parseInt(now);
                SharedPreferences.Editor editor = mSettings.edit();
                editor.putInt("sync", nnow);
                editor.apply();
            }
        }
    }

    public void syncContexts(UserContext user, int lastSync) {

        DBWorker db = new DBWorker(cnt);

        // 1.1 get all changes from some date
        try {
            RequestContext request = new RequestContext();
            request.setLogin(user.getEmail());
            request.setPass(user.getPassword());
            request.setMethod("GET");
            request.setUrl("http://irinn.us/api/v1.0/contexts/sync/" + lastSync);

            HTTPClientRequest async = new HTTPClientRequest(cnt);
            async.execute(request);
            ResponseContext response = async.get();
            switch (response.getResponseCode()) {
                case 200:
                    JSONArray contexts = response.getResponse().getJSONArray("contexts");
                    for (int i = 0; i < contexts.length(); i++) {
                        JSONObject context = contexts.getJSONObject(i);
                        JSONArray pointsIDs = context.getJSONArray("points");

                        ContextContext contextDB = new ContextContext();
                        contextDB.setUserID(user.getID());
                        contextDB.setTitle(context.getString("title"));
                        contextDB.setsID(context.getInt("id"));

                        contextDB.setlChSec(context.getInt("last_change"));

                        // add or update context
                        ContextContext DBCheck = db.getContextLastChangeFromSID(contextDB);
                        if (DBCheck.getlChSec() == 0) {
                            // insert new context
                            int id = db.addContext(contextDB);
                            contextDB.setID(id);
                            // insert all point to db
                            for (int k = 0; k < pointsIDs.length(); k++) {
                                int pID = pointsIDs.getInt(k);
                                getPointByServerID(user, pID, id);
                            }
                        } else if (DBCheck.getlChSec() < contextDB.getlChSec()) {
                            // update from server
                            db.updateContextFromServer(contextDB);
                            // remove and insert all points
                            db.removeAllContextPoints(DBCheck.getID());
                            for (int k = 0; k < pointsIDs.length(); k++) {
                                int pID = pointsIDs.getInt(k);
                                getPointByServerID(user, pID, DBCheck.getID());
                            }
                        } else {
                            // update on server
                            updateContext(contextDB, user);
                            // remove all point from server and add new
                            for (int k = 0; k < pointsIDs.length(); k++) {
                                int pID = pointsIDs.getInt(k);
                                PointContext pnt = new PointContext();
                                pnt.setsID(pID);
                                deletePoint(pnt, user);
                            }
                            ArrayList<PointContext> pnts = db.getPointsByContextID(DBCheck.getID());
                            for (int k = 0; k < pnts.size(); k++) {
                                addPoint(pnts.get(k), DBCheck, user);
                            }
                        }
                    }
                    break;
                case 400:
                    //Toast.makeText(cnt, "отсутствует обязательный параметр", Toast.LENGTH_LONG).show();
                    break;
                case 500:
                    //Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                    break;
                default:
                    //Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                    break;
            }
        } catch (JSONException ex) {
            Log.e("ERROR", ex.toString());
        } catch (ExecutionException ex) {
            Log.e("ERROR", ex.toString());
        } catch (InterruptedException ex) {
            Log.e("ERROR", ex.toString());
        }

        // 2.1 send all contexts without sID
        ArrayList<ContextContext> newLocalCont = db.getAllUnsyncContexts(user);
        if (newLocalCont.size() > 0) {
            for (int i = 0; i < newLocalCont.size(); i++) {
                ContextContext scont = addContext(newLocalCont.get(i).getTitle(), user);
                db.updateContext(scont);
                // 2.2 send all points for this context (find by local id)
                ArrayList<PointContext> newLocalPoints = db.getPointsByContextID(newLocalCont.get(i).getID());
                for (int k = 0; k < newLocalPoints.size(); k++) {
                    PointContext nPoint = addPoint(newLocalPoints.get(k), scont, user);
                    db.updatePoint(nPoint);
                }
            }
        }
    }

    public void syncTasks(UserContext user, int lastSync) {

        DBWorker db = new DBWorker(cnt);

        // 1.1 get all changes from some date
        try {
            RequestContext request = new RequestContext();
            request.setLogin(user.getEmail());
            request.setPass(user.getPassword());
            request.setMethod("GET");
            request.setUrl("http://irinn.us/api/v1.0/tasks/sync/" + lastSync);

            HTTPClientRequest async = new HTTPClientRequest(cnt);
            async.execute(request);
            ResponseContext response = async.get();
            switch (response.getResponseCode()) {
                case 200:
                    JSONArray tasks = response.getResponse().getJSONArray("tasks");
                    for (int i = 0; i < tasks.length(); i++) {
                        JSONObject task = tasks.getJSONObject(i);

                        String t = task.getString("subtask");
                        JSONArray subIDs = null;
                        if (t != "null")
                            subIDs = task.getJSONArray("subtask");

                        t = task.getString("contexts");
                        JSONArray t_cnt = null;
                        if (t != "null")
                            t_cnt = task.getJSONArray("contexts");

                        TaskContext taskDB = new TaskContext();

                        taskDB.setUserID(user.getID());
                        taskDB.setsID(task.getInt("id"));
                        taskDB.setlChSec(task.getInt("last_change"));
                        //taskDB.setLastChange(new Date(task.getInt("last_change") * 1000));
                        taskDB.setChecked(task.getInt("checked"));
                        taskDB.setDueDate(new Date((long) task.getInt("due_date") * 1000));
                        taskDB.setNotice(new Date((long) task.getInt("notice") * 1000));
                        taskDB.setRepeatID(task.getInt("repeat_id"));
                        taskDB.setText(task.getString("text"));
                        taskDB.setLocationNotice(task.getInt("location_notice"));

                        // add or update task
                        TaskContext DBCheck = db.getTaskLastChangeFromSID(taskDB);
                        if (DBCheck.getlChSec() == 0) {
                            // insert new context
                            int id = db.newTask(taskDB);
                            taskDB.setID(id);
                            // insert all subtasks to db if !null
                            if (subIDs != null) {
                                for (int k = 0; k < subIDs.length(); k++) {
                                    int pID = subIDs.getInt(k);
                                    getSubTaskByServerID(user, pID, id);
                                }
                            }
                            // insert relation
                            if (t_cnt != null) {
                                // add t_cnt
                                for (int l = 0; l < t_cnt.length(); l++) {
                                    int rid = t_cnt.getInt(l);
                                    db.saveTaskContextFromServer(id, rid);
                                }
                            }
                        } else if (DBCheck.getlChSec() < taskDB.getlChSec()) {
                            //  update from server
                            db.updateTaskFromServer(taskDB);
                            // remove and insert all points
                            db.removeAllSubtasks(DBCheck.getID());
                            if (subIDs != null) {
                                for (int k = 0; k < subIDs.length(); k++) {
                                    int subID = subIDs.getInt(k);
                                    getSubTaskByServerID(user, subID, DBCheck.getID());
                                }
                            }
                            // remove and insert all relations
                            for (int l = 0; l < t_cnt.length(); l++) {
                                int id = t_cnt.getInt(l);
                                db.saveTaskContextFromServer(DBCheck.getID(), id);
                            }
                        } else {
                            // update on server with relations
                            String cnts = db.getTaskContextServerIDs(DBCheck, user);
                            updateTask(DBCheck, user, cnts);
                            // remove all subtasks from server and add new
                            if (subIDs != null) {
                                for (int k = 0; k < subIDs.length(); k++) {
                                    int pID = subIDs.getInt(k);
                                    SubtaskContext pnt = new SubtaskContext();
                                    pnt.setsID(pID);
                                    deleteSubTask(pnt, user);
                                }
                            }
                            ArrayList<SubtaskContext> pnts = db.getSubTaskListByTaskID(DBCheck.getID());
                            for (int k = 0; k < pnts.size(); k++) {
                                addSubTask(pnts.get(k), user, DBCheck.getsID());
                            }
                        }
                    }
                    break;
                case 400:
                    //Toast.makeText(cnt, "отсутствует обязательный параметр", Toast.LENGTH_LONG).show();
                    break;
                case 500:
                    //Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                    break;
                default:
                    //Toast.makeText(cnt, "ошибка БД на сервере", Toast.LENGTH_LONG).show();
                    break;
            }
        } catch (JSONException ex) {
            Log.e("ERROR", ex.toString());
        } catch (ExecutionException ex) {
            Log.e("ERROR", ex.toString());
        } catch (InterruptedException ex) {
            Log.e("ERROR", ex.toString());
        }

        // 2.1 send all tasks without sID
        ArrayList<TaskContext> newLocalTasks = db.getAllUnsyncTask(user);
        if (newLocalTasks.size() > 0) {
            for (int i = 0; i < newLocalTasks.size(); i++) {
                String cnts = db.getTaskContextServerIDs(newLocalTasks.get(i), user);
                int tsID = addTask(newLocalTasks.get(i), user, cnts);
                db.changeTask(newLocalTasks.get(i));
                ArrayList<SubtaskContext> pnts = db.getSubTaskListByTaskID(newLocalTasks.get(i).getID());
                for (int k = 0; k < pnts.size(); k++) {
                    addSubTask(pnts.get(k), user, newLocalTasks.get(i).getsID());
                }
            }
        }

    }


    public boolean hasInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager) cnt.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm == null) {
            return false;
        }
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        if (netInfo == null) {
            return false;
        }
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected()) {
                    Log.d(this.toString(), "test: wifi conncetion found");
                    return true;
                }
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected()) {
                    Log.d(this.toString(), "test: mobile connection found");
                    return true;
                }
        }
        return false;
    }

    public int getAutoContext(UserContext user, String text) {
        int serverContextID = 0;
        try {
            RequestContext request = new RequestContext();
            request.setLogin(user.getEmail());
            request.setPass(user.getPassword());
            request.setMethod("POST");
            request.setUrl("http://irinn.us/api/v1.0/tasks/auto/");

            JSONObject js = new JSONObject();
            js.put("text", text);
            request.setJsonData(js);

            HTTPClientRequest async = new HTTPClientRequest(cnt);
            async.execute(request);
            ResponseContext response = async.get();
            switch (response.getResponseCode()) {
                case 200:
                    serverContextID = response.getResponse().getInt("context");
                    break;
                default: break;
            }
        } catch (JSONException ex) { Log.e("ERROR", ex.toString());}
        catch (ExecutionException ex) {
            Log.e("ERROR", ex.toString());
        } catch (InterruptedException ex) {
            Log.e("ERROR", ex.toString());
        }

        return serverContextID;

    }


    class MySynq extends AsyncTask<UserContext, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //запускаем диалог показывающий что ты работаешь во всю
        }

        @Override
        protected Void doInBackground(UserContext... params) {
            //вот тут пишем весь кошмар и ужас который будет выполнять в отдельном потоке, короче что угодно.
            makeSync(params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            //Toast.makeText(cnt, "Синхронизация", Toast.LENGTH_SHORT).show();
            //а здесь мы прячем диалог и заканчиваем работу всех функций которые были запущены в doInBackground()
        }
    }


    class MyFullSynq extends AsyncTask<UserContext, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //запускаем диалог показывающий что ты работаешь во всю
        }

        @Override
        protected Void doInBackground(UserContext... params) {
            //вот тут пишем весь кошмар и ужас который будет выполнять в отдельном потоке, короче что угодно.
            makeFullSync(params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            Toast.makeText(cnt, "Синхронизация", Toast.LENGTH_SHORT).show();
            //а здесь мы прячем диалог и заканчиваем работу всех функций которые были запущены в doInBackground()
        }
    }

    public void callSync(UserContext user){
        MySynq task = new MySynq();
        task.execute(user);
    }

    public void callFullSync(UserContext user){
        MyFullSynq task = new MyFullSynq();
        task.execute(user);
    }
}
