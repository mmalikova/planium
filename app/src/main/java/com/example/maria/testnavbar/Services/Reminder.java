package com.example.maria.testnavbar.Services;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.example.maria.testnavbar.Provider.TaskContext;

/**
 * Created by Maria on 27.04.2015.
 */
public class Reminder {

    Activity a;
    final static int RQS_TIME = 1;
    //DBWorker db;

    public Reminder(Activity a){
        this.a = a;
        //db = new DBWorker(a);
    }

    public void setAlarm(TaskContext task){


        Intent intent = new Intent(a, AlarmReciever.class);
        intent.setAction(Integer.toString(task.getID()));
        PendingIntent pendingIntent = PendingIntent.getBroadcast(a, RQS_TIME, intent, 0);
        AlarmManager alarmManager = (AlarmManager) a.getSystemService(Context.ALARM_SERVICE);

        switch(task.getRepeatID()){
            case 0:
                alarmManager.set(AlarmManager.RTC_WAKEUP, task.getNotice().getTime(), pendingIntent);
                break;
            case 1:
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,  task.getNotice().getTime(), 86400000, pendingIntent);
                break;
            case 2:
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,  task.getNotice().getTime(), 604800000, pendingIntent);
                break;
            case 3:
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,  task.getNotice().getTime(), 886400000, pendingIntent);
                break;
            case 4:
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,  task.getNotice().getTime(), 900000000, pendingIntent);
                break;
            default: break;
        }

        alarmManager.set(AlarmManager.RTC_WAKEUP, task.getNotice().getTime(), pendingIntent);
        //alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,  targetCal.getTimeInMillis(), 7000000, pendingIntent);
    }

    public void cancelAlarm(TaskContext task){
        Intent intent = new Intent(a, AlarmReciever.class);
        intent.setAction(Integer.toString(task.getID()));
        PendingIntent pendingIntent = PendingIntent.getBroadcast(a, RQS_TIME, intent, 0);
        AlarmManager alarmManager = (AlarmManager) a.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
    }
}


/*
* ежедневно - 86400000
* еженедельно - 604800000
* ежемесячно
* ежегодно - 31536000000
* */