package com.example.maria.testnavbar.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;

import com.example.maria.testnavbar.Provider.TaskContext;
import com.example.maria.testnavbar.R;

import java.util.ArrayList;
import java.util.Calendar;


public class TaskAdapter extends ArrayAdapter<TaskContext> {
    private final Activity activity;
    //
    private final ArrayList<TaskContext> entries;

    // конструктор класса, принимает активность, листвью и массив данных
    public TaskAdapter(final Activity a, final int textViewResourceId, final ArrayList<TaskContext> entries) {
        super(a, textViewResourceId, entries);
        this.entries = entries;
        activity = a;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        ViewHolder holder;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.item_task_main_list, parent, false);
            holder = new ViewHolder();
            // инициализируем нашу разметку
            holder.checkBox = (CheckBox) v.findViewById(R.id.checkBox_main_list_item);
            //holder.textView = (TextView) v.findViewById(R.id.textView_list_item_id);

            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        TaskContext task = entries.get(position);
        if (task != null) {
            // получаем текст из массива
            holder.checkBox.setText(task.getText());
            if (task.getChecked() == 1){
                holder.checkBox.setChecked(true);
            }
            long curDate = Calendar.getInstance().getTimeInMillis()/1000;
            long due=0;
            if (task.getDueDate()!=null)
                due = task.getDueDate().getTime()/1000;
            if (due>0 && due<curDate) {
                holder.checkBox.setTextColor(Color.RED);
            } else {
                holder.checkBox.setTextColor(Color.DKGRAY);
            }
            //holder.textView.setText(task.getID());
        }
        return v;
    }

    // для быстроты вынесли в отдельный класс
    private static class ViewHolder {
        public CheckBox checkBox;
        //public TextView textView;
    }
}