package com.example.maria.testnavbar.Fragments;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.example.maria.testnavbar.Adapters.PointListAdapter;
import com.example.maria.testnavbar.Provider.ContextContext;
import com.example.maria.testnavbar.Provider.DBWorker;
import com.example.maria.testnavbar.Provider.PointContext;
import com.example.maria.testnavbar.Provider.UserContext;
import com.example.maria.testnavbar.R;
import com.example.maria.testnavbar.RestAPI.WebWorker;


import java.util.ArrayList;

public class Location extends Fragment {

    /*private static final long POINT_RADIUS = 500; // в метрах
    private static final long PROX_ALERT_EXPIRATION = -1; // не истекает по времени
    private static final String PROX_ALERT_INTENT = "com.example.maria.testnavbar.Fragments.Location";*/

    private DBWorker db;
    private UserContext activeUser;
    private ContextContext context;
    private Boolean isNew = true;
    private int id;
    private ImageButton newPointBTN;
    boolean hasReceiver = false;

    private EditText contextTitle;
    private ListView lv;

    private PointListAdapter adapter;

    private ArrayList<PointContext> pointList;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_location_view, container,false);
        db = new DBWorker(getActivity());
        activeUser = db.checkUser();

        lv = (ListView)view.findViewById(R.id.listView_context);
        contextTitle = (EditText)view.findViewById(R.id.new_location_title);
        newPointBTN = (ImageButton)view.findViewById(R.id.btn_newPoint);
        newPointBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddPoint();
            }
        });
        context = new ContextContext();

        pointList = new ArrayList<PointContext>();


        Bundle bundle = getArguments();
        if (bundle != null) {
            id = bundle.getInt("contextID");
            isNew = false;
            context = db.getContextByID(id);
            pointList = db.getPointsByContextID(id);
            contextTitle.setText(context.getTitle());
        }
        else
        {
            context.setUserID(activeUser.getID());
            pointList = db.getTempPoints();
        }

        adapter = new PointListAdapter(getActivity(), R.id.listViewLocations, pointList);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                openPoint(position);
            }
        });

        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,int position, long id) {
                delPoint(position);
                return true;
            }
        });

        return view;
    }

    public void delPoint(int position){
        final int pos = position;
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final String[] actions ={"Удалить"};
        builder.setTitle("Возможные действия");

        builder.setItems(actions, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                // toDo remove point from server

                WebWorker www = new WebWorker(getActivity());
                if ((pointList.get(pos).getsID()>0)){
                    www.deletePoint(pointList.get(pos), activeUser);
                }

                db.removePoint(pointList.get(pos).getID());
                pointList.remove(pos);



                adapter.notifyDataSetChanged();
            }
        });
        builder.setCancelable(true);
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void AddPoint(){

        Point yfc = new Point();
        if(context.getID() != 0){
            Bundle bundle = new Bundle();
            bundle.putInt("contextID", context.getID());
            yfc.setArguments(bundle);
        }

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, yfc).addToBackStack(null).commit();
    }

    public void openPoint(int position){
        Point yfc = new Point();
        Bundle bundle = new Bundle();
        bundle.putInt("pointID", pointList.get(position).getID());
        yfc.setArguments(bundle);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame,  yfc).addToBackStack(null).commit();
    }

    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.new_element, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                if (contextTitle.getText().length()>0 && pointList.size()>0){
                    context.setTitle(contextTitle.getText().toString());
                    if (isNew) {

                        WebWorker www = new WebWorker(getActivity());
                        ContextContext newCnt = www.addContext(contextTitle.getText().toString() , activeUser);

                        if (newCnt.getsID()>0)
                            context.setsID(newCnt.getsID());
                        int cnt_id = db.addContext(context);


                        // добавляем все точки на сервер
                        for (int i=0; i<pointList.size(); i++){
                            PointContext pnt = www.addPoint(pointList.get(i), newCnt, activeUser);
                            if (pnt.getsID()>0)
                                pointList.get(i).setsID(pnt.getsID());
                        }

                        db.updatePoints(pointList, cnt_id);
                        //setProximityAlert();




                        getActivity().recreate();

                    } else {

                        WebWorker www = new WebWorker(getActivity());
                        boolean rez = www.updateContext(context, activeUser);


                        db.updateContext(context);

                        // todo update pointList on server
                        db.updatePoints(pointList, context.getID());

                        //setProximityAlert();
                        getActivity().recreate();
                    }
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.content_frame, new LocationList()).addToBackStack(null).commit();
                }
                else {
                    Toast.makeText(getActivity(), "Ошибка при заполнении полей", Toast.LENGTH_SHORT).show();
                }

                return true;
            case R.id.action_cancel:
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_frame, new LocationList()).addToBackStack(null).commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




}

