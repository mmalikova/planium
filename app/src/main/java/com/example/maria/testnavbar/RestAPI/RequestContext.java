package com.example.maria.testnavbar.RestAPI;

import org.json.JSONObject;

public class RequestContext {

    private String method; // GET POST PUT DELETE
    private String login = "";
    private String pass = "";
    private String url;
    private JSONObject jsonData;

    public String getMethod() {  return method;}
    public String getLogin() { return login; }
    public String getPass() { return pass; }
    public String getUrl() { return url; }
    public JSONObject getJsonData() { return jsonData; }

    public void setMethod(String method) { this.method = method; }
    public void setLogin(String login) { this.login = login; }
    public void setPass(String pass) { this.pass = pass; }
    public void setUrl(String url) { this.url = url; }
    public void setJsonData(JSONObject jsonData) { this.jsonData = jsonData; }
}
