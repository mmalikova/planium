package com.example.maria.testnavbar.RestAPI;

import org.json.JSONObject;

public class ResponseContext {

    private int responseCode;
    private JSONObject response;

    public int getResponseCode() { return responseCode; }
    public JSONObject getResponse() { return response; }

    public void setResponseCode(int responseCode) { this.responseCode = responseCode; }
    public void setResponse(JSONObject response) { this.response = response; }
}
