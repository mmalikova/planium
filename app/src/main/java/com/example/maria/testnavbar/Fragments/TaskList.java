package com.example.maria.testnavbar.Fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.maria.testnavbar.Adapters.TaskAdapter;
import com.example.maria.testnavbar.Provider.ContextContext;
import com.example.maria.testnavbar.Provider.DBWorker;
import com.example.maria.testnavbar.Provider.TaskContext;
import com.example.maria.testnavbar.Provider.UserContext;
import com.example.maria.testnavbar.R;
import com.example.maria.testnavbar.RestAPI.WebWorker;

import java.util.ArrayList;


public class TaskList extends Fragment {
    private TaskAdapter adapter;
    ArrayList<TaskContext> tasks;
    private ListView lv;
    private DBWorker db;
    String location;

    UserContext activeUser;


    public TaskList(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.list_tasks, container, false);

        db = new DBWorker(getActivity());
        activeUser = db.checkUser();

       // ActionBar bar = getActivity().getActionBar();
       // bar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        // If Bundle is set
        tasks = new ArrayList<TaskContext>();
        Bundle bundle = getArguments();
        if (bundle != null) {
            location = bundle.getString("tag");
            if (location != null){
            switch (location){
                case "all":
                    tasks = db.getAllTasks(activeUser);
                    getActivity().setTitle("Все задачи");
                    break;
                case "without":
                    tasks = db.getTasksWithoutContext(activeUser);
                    getActivity().setTitle("Без контекста");
                    break;
                case "today":
                    tasks = db.getTodayTasks(activeUser.getID());
                    getActivity().setTitle("Сегодня");
                    break;
                default:
                    ContextContext cnt = db.getContextByTitle(location, activeUser.getID());
                    tasks = db.getTasksByContext(cnt.getID());
                    getActivity().setTitle(location);
                    break;
            }}
        }
        else{
            getActivity().setTitle("Все задачи");
            tasks = db.getAllTasks(activeUser);
        }

        lv = (ListView)view.findViewById(R.id.listView1);
        adapter = new TaskAdapter(getActivity(), R.id.listView1, tasks);
        lv.setAdapter(adapter);


        // Обработка события на клик по элементу списка
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //открываем другой фрагмент
                Task yfc = new Task();
                Bundle bundle = new Bundle();
                bundle.putInt("taskID", tasks.get(position).getID());
                yfc.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_frame,  yfc).commit();
            }
        });

        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,int position, long id) {
                final int pos = position;
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                final String[] actions ={"Удалить"};
                builder.setTitle("Возможные действия");

                builder.setItems(actions, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {

                        db.deleteTask(tasks.get(pos));
                        if (tasks.get(pos).getsID()>0) {
                            WebWorker www = new WebWorker(getActivity());
                            www.deleteTask(tasks.get(pos), activeUser);
                        }

                        tasks.remove(pos);
                        adapter.notifyDataSetChanged();
                    }
                });
                builder.setCancelable(true);
                AlertDialog alert = builder.create();
                alert.show();

                return true;
            }
        });

        return view;
    }



    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.task_list, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_new_note:

                //переходим на фрагмент добавления положения
                Task yfc = new Task();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_frame,  yfc).commit();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
