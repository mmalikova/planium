package com.example.maria.testnavbar.Fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.maria.testnavbar.Adapters.SeparatedTaskAdapter;
import com.example.maria.testnavbar.Provider.DBWorker;
import com.example.maria.testnavbar.Provider.SeparatedTask;
import com.example.maria.testnavbar.Provider.UserContext;
import com.example.maria.testnavbar.R;

import java.util.ArrayList;


public class SeparatedFragment extends Fragment {
    private SeparatedTaskAdapter adapter;
    ArrayList<SeparatedTask> tasks;
    private ListView lv;
    private DBWorker db;
    //String location;

    UserContext activeUser;


    public SeparatedFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.list_tasks, container, false);

        db = new DBWorker(getActivity());
        activeUser = db.checkUser();

        tasks = db.getSeparatedTask(activeUser.getID());

        lv = (ListView)view.findViewById(R.id.listView1);
        adapter = new SeparatedTaskAdapter(getActivity(), R.id.listView1, tasks);
        lv.setAdapter(adapter);

        getActivity().setTitle("Неделя");


        // Обработка события на клик по элементу списка
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //открываем другой фрагмент
                Task yfc = new Task();
                Bundle bundle = new Bundle();
                bundle.putInt("taskID", tasks.get(position).getID());
                yfc.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_frame,  yfc).commit();
            }
        });

        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,int position, long id) {
                final int pos = position;
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                final String[] actions ={"Удалить"};
                builder.setTitle("Возможные действия");

                builder.setItems(actions, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        db.deleteTask(tasks.get(pos).getID(), activeUser.getID());
                        tasks.remove(pos);
                        adapter.notifyDataSetChanged();
                    }
                });
                builder.setCancelable(true);
                AlertDialog alert = builder.create();
                alert.show();

                return true;
            }
        });

        return view;
    }



    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.task_list, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_new_note:

                //переходим на фрагмент добавления положения
                Task yfc = new Task();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_frame,  yfc).commit();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
