package com.example.maria.testnavbar.Provider;

import java.util.Date;

public class ContextContext {
    private int ID;
    private int sID;
    private int userID;
    private String title;
    private Date lastChange;
   //private long lChange;

    public int getlChSec() {
        return lChSec;
    }

    public void setlChSec(int lChSec) {
        this.lChSec = lChSec;
    }

    private int lChSec;

    public ContextContext() {}

    public int getID() {return this.ID;}
    public int getsID() {return this.sID;}
    public int getUserID() {return this.userID;}
    public String getTitle() {return this.title;}
    public Date getLastChange() {return this.lastChange;}
    //public long getlChange() {return this.lChange;}


    public void setID(int ID) {this.ID = ID;}
    public void setsID(int SID) {this.sID = SID;}
    public void setUserID(int USERID) {this.userID = USERID;}
    public void setTitle(String TITLE) {this.title = TITLE;}
    //public void setlChange(long LASTCHANGE) {this.lChange = LASTCHANGE;}
    public void setLastChange(Date LASTCHANGE) {this.lastChange = LASTCHANGE;}


}
