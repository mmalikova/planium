package com.example.maria.testnavbar.Fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;


import com.example.maria.testnavbar.Adapters.DatePickerDue;
import com.example.maria.testnavbar.Adapters.DatePickerReminder;
import com.example.maria.testnavbar.Adapters.SubtaskAdapter;
import com.example.maria.testnavbar.Adapters.TimePickerReminder;
import com.example.maria.testnavbar.Provider.ContextContext;
import com.example.maria.testnavbar.Provider.DBWorker;
import com.example.maria.testnavbar.Provider.SubtaskContext;
import com.example.maria.testnavbar.Provider.TaskContext;
import com.example.maria.testnavbar.Provider.UserContext;
import com.example.maria.testnavbar.R;
import com.example.maria.testnavbar.RestAPI.WebWorker;
import com.example.maria.testnavbar.Services.Reminder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class Task extends Fragment {
    SimpleDateFormat format;
    SimpleDateFormat format_time;
    private EditText taskText;
    private CheckBox taskCheck;
    private TextView selLocations;
    private EditText newSubTask;
    private ListView SubtasksListView;
    private ImageButton addSubTask;
    private CheckBox setAutoContext;

    private EditText dueDate;

    private CheckBox setReminder;
    private CheckBox contextReminder;
    private EditText reminderDate;

    private EditText reminderTime;
    private Spinner repeat;


    private ArrayList<ContextContext> fetch = new ArrayList<ContextContext>();
    private String[] checkLocations;
    private boolean[] mCheckedItems;
    private ArrayList<String> newContext;
    private ArrayList<SubtaskContext> subtasks;

    int DIALOG_DATE = 1;

    private DBWorker db;
    private UserContext activeUser;
    private TaskContext task = new TaskContext();
    private SubtaskAdapter adapter;

    private String contextServerIDs;

    private int tabListPos;

    String str = "";
    int pos;
    Boolean isNew = true;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_task, container,false);

        format = new SimpleDateFormat("dd/MM/yyyy");
        format_time = new SimpleDateFormat("HH:mm");

        taskText = (EditText)view.findViewById(R.id.editText_taskText);
        taskCheck = (CheckBox)view.findViewById(R.id.Check_taskText);
        setAutoContext = (CheckBox)view.findViewById(R.id.checkBox);
        selLocations = (TextView)view.findViewById(R.id.textView2);
        newSubTask = (EditText) view.findViewById(R.id.newSubTask);
        addSubTask = (ImageButton) view.findViewById(R.id.addSubTask);
        SubtasksListView = (ListView) view.findViewById(R.id.subtasks);
        //btn2 = (ImageButton) view.findViewById(R.id.imageButton);
        //clearDate = (ImageButton) view.findViewById(R.id.clearDate);
        dueDate = (EditText) view.findViewById(R.id.dueDate);

        setReminder = (CheckBox)view.findViewById(R.id.setReminder);
        contextReminder = (CheckBox)view.findViewById(R.id.contextReminder);
        reminderDate = (EditText)view.findViewById(R.id.reminderDate);

        reminderTime = (EditText)view.findViewById(R.id.reminderTime);

        repeat = (Spinner) view.findViewById(R.id.repeat);

        db = new DBWorker(getActivity());
        activeUser = db.checkUser();
        subtasks = new ArrayList<SubtaskContext>();
        checkLocations = db.getContextNameList(activeUser.getID());
        mCheckedItems = new boolean[checkLocations.length];

        getActivity().setTitle("Задача");

        Bundle bundle = getArguments();
        if (bundle != null) {
            pos = bundle.getInt("taskID");
            tabListPos = bundle.getInt("tabListPos");

            if (pos != 0) {
                isNew = false;
                task.setID(pos);
                task = db.getTaskById(pos);

                taskText.setText(task.getText());
                if (task.getChecked() == 1)
                    taskCheck.setChecked(true);

                // список привязанных локаций
                mCheckedItems = db.getSelectedContext(task.getID(), activeUser.getID());
                newContext = new ArrayList<String>();
                // генерируем строку
                for (int i = 0; i < mCheckedItems.length; i++) {
                    if (mCheckedItems[i]) {
                        str += checkLocations[i] + "   ";
                        newContext.add(checkLocations[i]);
                    }
                }
                if (str.length() > 0)
                    selLocations.setText(str);

                if (task.getDueDate() !=null && task.getDueDate().getTime()!=0){
                    // duedate
                    dueDate.setText(format.format(task.getDueDate()));
                }


                if (task.getNotice() !=null && task.getNotice().getTime()!=0){
                    setReminder.setChecked(true);
                    reminderDate.setText(format.format(task.getNotice()));
                    reminderTime.setText(format_time.format(task.getNotice()));
                }

                if (task.getLocationNotice()==1){
                    contextReminder.setChecked(true);
                }

                // trying to get subtasks
                subtasks = db.getSubTaskListByTaskID(pos);
                repeat.setSelection(task.getRepeatID());
            }

        }

        adapter = new SubtaskAdapter(getActivity(), R.id.listView1, subtasks);
        SubtasksListView.setAdapter(adapter);
        setListViewHeightBasedOnChildren(SubtasksListView);


        addSubTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // add subtask to list
                if (newSubTask.getText() != null){
                    SubtaskContext nst = new SubtaskContext();
                    nst.setText(newSubTask.getText().toString());
                    subtasks.add(nst);
                    adapter.notifyDataSetChanged();
                    newSubTask.setText("");
                    setListViewHeightBasedOnChildren(SubtasksListView);

                    // add subTask on server
                    if (task.getsID()>0){
                        WebWorker www = new WebWorker(getActivity());
                        www.addSubTask(nst, activeUser, task.getsID());
                    }
                }
            }
        });

        dueDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    DialogFragment dateDialog = new DatePickerDue();
                    dateDialog.show(getActivity().getSupportFragmentManager(), "datePicker");
                }else {

                }
            }
        });

        setAutoContext.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // TODO Auto-generated method stub
                if(isChecked){
                    // todo try to get context from server
                    WebWorker www = new WebWorker(getActivity());
                    if (www.hasInternetConnection() && taskText.getText().length()>0) {
                        int serverCnt = www.getAutoContext(activeUser, taskText.getText().toString());
                        if (serverCnt>0){
                            mCheckedItems = db.getSelectedContextAuto(activeUser.getID(), serverCnt);
                            str = "";
                            newContext = new ArrayList<String>();
                            // генерируем строку
                            for (int i = 0; i < mCheckedItems.length; i++) {
                                if (mCheckedItems[i]) {
                                    str += checkLocations[i] + "   ";
                                    newContext.add(checkLocations[i]);
                                }
                            }
                            selLocations.setText(str);
                        }
                    }
                }
            }
        });

        dueDate.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                DialogFragment dateDialog = new DatePickerDue();
                dateDialog.show(getActivity().getSupportFragmentManager(), "datePicker");
            }

        });

       /* clearDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dueDate.setText("");
            }
        });*/

        reminderDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    DialogFragment dateDialog = new DatePickerReminder();
                    dateDialog.show(getActivity().getSupportFragmentManager(), "datePicker");
                }else {

                }
            }
        });


        reminderDate.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                DialogFragment dateDialog = new DatePickerReminder();
                dateDialog.show(getActivity().getSupportFragmentManager(), "datePicker");
            }

        });



        reminderTime.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    DialogFragment dateDialog = new TimePickerReminder();
                    dateDialog.show(getActivity().getSupportFragmentManager(), "datePicker");
                }else {

                }
            }
        });


        reminderTime.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                DialogFragment dateDialog = new TimePickerReminder();
                dateDialog.show(getActivity().getSupportFragmentManager(), "datePicker");
            }

        });



        ArrayList<ContextContext> contexts = db.getAllContexts(activeUser.getID());

        ContextContext EmptyPoint = new ContextContext();
        EmptyPoint.setTitle("Без контекста");
        fetch.add(EmptyPoint);

        for (int i = 0; i<contexts.size(); i++){
            fetch.add(contexts.get(i));
        }

        selLocations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                builder.setTitle("Выберите контекст")
                        .setCancelable(false)
                        .setMultiChoiceItems(checkLocations, mCheckedItems,
                                new DialogInterface.OnMultiChoiceClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which, boolean isChecked) {
                                        mCheckedItems[which] = isChecked;
                                    }
                                })
                        .setPositiveButton("Готово",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        StringBuilder state = new StringBuilder();
                                        newContext = new ArrayList<String>();
                                        for (int i = 0; i < checkLocations.length; i++) {

                                            if (mCheckedItems[i]){

                                                newContext.add(checkLocations[i]);
                                                state.append(checkLocations[i] + "   ");
                                            }

                                        }
                                        if (state.toString().length()>0) {
                                            // устанавливаем название
                                            selLocations.setText(state.toString());
                                        } else {

                                            selLocations.setText("Без контекста");
                                        }
                                    }
                                })
                        .setNegativeButton("Отмена",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        dialog.cancel();

                                    }
                                });

                AlertDialog alert = builder.create();
                alert.show();

            }

        });
        return view;
    }

    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.new_element, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                SaveNote();
                return true;
            case R.id.action_cancel:
                //TODO kill fragment, go to previous
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                //fragmentManager.beginTransaction().replace(R.id.content_frame, new TaskList()).addToBackStack(null).commit();
                fragmentManager.beginTransaction().remove(this).commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void SaveNote(){
        if (taskText.getText().length() > 0) {
            // текст задачи
            task.setText(taskText.getText().toString());

            // срок
            if (dueDate.getText().length()>0){

                try {
                    task.setDueDate(format.parse(dueDate.getText().toString()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            else {
                task.setDueDate(null);
            }

            // напоминание
            if(setReminder.isChecked()){
                if (reminderDate.getText().length()>0 && reminderTime.getText().length()>0){
                    try {
                        String[] parts = reminderTime.getText().toString().split(":");
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(format.parse(reminderDate.getText().toString()));
                        cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(parts[0]));
                        cal.set(Calendar.MINUTE,Integer.parseInt(parts[1]));


                        Date d = cal.getTime();
                        task.setNotice(d);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                    task.setRepeatID(repeat.getSelectedItemPosition());
                    // устанавливаем напоминание
                    Reminder rm = new Reminder(getActivity());
                    rm.setAlarm(task);
                }
            }
            else {
                task.setNotice(null);
                // cancel system notice
                Reminder rm = new Reminder(getActivity());
                rm.cancelAlarm(task);
            }

            // уведомление при смене контекста
            if (contextReminder.isChecked()){
                task.setLocationNotice(1);
            }
            else  task.setLocationNotice(0);

            if (taskCheck.isChecked()){
                task.setChecked(1);
            }
            else task.setChecked(0);

            if (isNew) {

                String sCon = db.getContextServerIDs(newContext, activeUser);

                // add task on server
                WebWorker www = new WebWorker(getActivity());
                int taskSId = www.addTask(task, activeUser, sCon);

                if (taskSId>0){
                    task.setsID(taskSId);
                    for (int i =0; i<subtasks.size(); i++){
                        www.addSubTask(subtasks.get(i), activeUser, taskSId);
                    }
                }
                task.setUserID(activeUser.getID());
                task.setID(db.newTask(task));
            } else {
                String sCon = db.getContextServerIDs(newContext, activeUser);
                WebWorker www = new WebWorker(getActivity());
                www.updateTask(task, activeUser, sCon);
                db.changeTask(task);

                // remove checked subtasks from server
                for (int i =0 ; i<subtasks.size(); i++){
                    if (subtasks.get(i).isChecked()){
                        www.deleteSubTask(subtasks.get(i), activeUser);
                    }
                }
            }
            db.saveTaskContext(newContext, task);
            db.saveAllSubtaskToTask(subtasks, task);

            // remove all checked subTasks from server


            TaskList yfc = new TaskList();
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, yfc).commit();
        }
    }


    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, RelativeLayout.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }




}
