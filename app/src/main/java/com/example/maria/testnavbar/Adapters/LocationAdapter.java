package com.example.maria.testnavbar.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.maria.testnavbar.Provider.ContextContext;
import com.example.maria.testnavbar.R;

import java.util.ArrayList;


public class LocationAdapter  extends ArrayAdapter<ContextContext> {

    private final FragmentActivity activity;
    //
    private final ArrayList<ContextContext> entries;

    // конструктор класса, принимает активность, листвью и массив данных
    public LocationAdapter(final Activity a, final int textViewResourceId, final ArrayList<ContextContext> entries) {
        super(a, textViewResourceId, entries);
        this.entries = entries;
        activity = (FragmentActivity)a;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        ViewHolder holder;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.item_location_list, parent, false);
            holder = new ViewHolder();
            // инициализируем нашу разметку
            holder.Text = (TextView) v.findViewById(R.id.location_item_text);
          // holder.Text.setOnClickListener(listener);
            //holder.Text.setTag(position);
           // holder.Right = (ImageView) v.findViewById(R.id.right);
            //holder.Right.setOnClickListener(listener);
            //holder.Right.setTag(position);
           // holder.Delete = (ImageView) v.findViewById(R.id.deleteLocation);
           // holder.Delete.setOnClickListener(listener);


            // holder.noteId = (TextView) v.findViewById(R.id.noteID);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        ContextContext locations = entries.get(position);
        if (locations != null) {
            // получаем текст из массива
            holder.Text.setText(locations.getTitle());
            holder.Text.setTag(locations.getTitle());
            //holder.Right.setTag(locations.getTitle());
            //holder.Delete.setTag(locations.getTitle());

        }
        return v;
    }

    // для быстроты вынесли в отдельный класс
    private static class ViewHolder {

        public TextView Text;
        //public ImageView Right;
        //public ImageView Delete;
        //public TextView noteId;
    }

   /* private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch(v.getId()) {
                case R.id.location_item_text:
                    Toast.makeText(activity, "text button Clicked",
                            Toast.LENGTH_LONG).show();
                    break;
                case R.id.right:
                    Toast.makeText(activity, "Right button Clicked",
                            Toast.LENGTH_LONG).show();
                    Location yfc = new Location();
                    Bundle bundle = new Bundle();
                    bundle.putString("tag", v.getTag().toString());
                    yfc.setArguments(bundle);

                    //FragmentManager fragmentManager = activity.getParent().getSupportFragmentManager();
                    FragmentManager fragmentManager = activity.getSupportFragmentManager();

                    fragmentManager.beginTransaction().replace(R.id.content_frame,  yfc).commit();
                    break;
                case R.id.deleteLocation:
                    //TODO: delete item from db and updateList
                    String title = v.getTag().toString();
                    DBWorker db = new DBWorker(activity.getApplication());
                    db.removeLocationByTitle(title);
                    // get index of location in entries
                    int id = 0;
                    for (int i = 0; i<entries.size(); i++) {
                        if (entries.get(i).getTitle() == title ){
                            id = i;
                        }
                    }
                    entries.remove(id);
                    notifyDataSetChanged();

                    /*int list_item_id = Integer.parseInt(v.getTag().toString()); // ид
                    String selectedItem = parent.getItemAtPosition(position).toString();

                    mAdapter.remove(selectedItem);
                    mAdapter.notifyDataSetChanged();*/

                    /*Toast.makeText(activity, "элемент удален!",
                            Toast.LENGTH_LONG).show();
                    break;
                default:
                    break;
            }

        }
    };*/
}
