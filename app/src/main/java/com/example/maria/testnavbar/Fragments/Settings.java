package com.example.maria.testnavbar.Fragments;

import android.os.Bundle;

//import android.support.v4.preference.PreferenceFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.maria.testnavbar.R;
import com.example.maria.testnavbar.View.PreferenceFragment;

/**
 * Created by Maria on 07.05.2015.
 */
public class Settings  extends PreferenceFragment {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
        getActivity().setTitle("Настройки");
    }
}
