package com.example.maria.testnavbar.Activities;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Toast;

import com.example.maria.testnavbar.Fragments.Settings;
import com.example.maria.testnavbar.Fragments.Task;
import com.example.maria.testnavbar.Provider.DBWorker;
import com.example.maria.testnavbar.Provider.SeparatedTask;
import com.example.maria.testnavbar.Provider.UserContext;
import com.example.maria.testnavbar.Fragments.LocationList;
import com.example.maria.testnavbar.Fragments.SeparatedFragment;
import com.example.maria.testnavbar.Fragments.TaskList;
import com.example.maria.testnavbar.R;
import com.example.maria.testnavbar.RestAPI.WebWorker;
import com.example.maria.testnavbar.Services.ContextNotify;

import com.google.android.gms.common.GooglePlayServicesUtil;
import com.mikepenz.iconics.typeface.FontAwesome;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeader;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.SectionDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.mikepenz.materialdrawer.model.interfaces.Nameable;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends ActionBarActivity implements
        TextToSpeech.OnInitListener {

    private Drawer.Result drawerResult = null;
    private Drawer drawer = null;
    public static FragmentManager fragmentManager;
    DBWorker db;
    UserContext activeUser;
    final static int RQS_TIME = 1;
    private TextToSpeech mTTS;

    @Override
    public void onInit(int status) {
        // TODO Auto-generated method stub
        if (status == TextToSpeech.SUCCESS) {

            Locale locale = new Locale("ru");

            int result = mTTS.setLanguage(locale);
            //int result = mTTS.setLanguage(Locale.getDefault());

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "Извините, этот язык не поддерживается");
            } else {
               // mButton.setEnabled(true);
            }

        } else {
            Log.e("TTS", "Ошибка!");
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        InitializeComponents();



        // ели пришли с виджета - открываем фрагмент с нужной задачей pos = bundle.getInt("taskID");
        Fragment fragment;
        Intent intent = getIntent();
        int taskID = intent.getIntExtra("task", -1);
        if (taskID >= 0){
            Bundle bundle = new Bundle();
            bundle.putInt("taskID", taskID);
            fragment = new Task();
            fragment.setArguments(bundle);

        } else if (taskID== -10){
            fragment = new Task();
        } else {
            fragment = new TaskList();
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).addToBackStack(null).commit();

        timerSync();
        timerLocation();

        mTTS = new TextToSpeech(this, this);

    }

    @Override
    public void onBackPressed() {
        // Закрываем Navigation Drawer по нажатию системной кнопки "Назад" если он открыт
        if (drawerResult.isDrawerOpen()) {
            drawerResult.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    // Заглушка, работа с меню
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    // Заглушка, работа с меню
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    public void InitializeComponents() {
        db = new DBWorker(this);
        activeUser = db.checkUser();
        final String[] titlesList = db.getContextNameList(activeUser.getID());

        ArrayList<SeparatedTask> test =  db.getSeparatedTask(activeUser.getID());

        Integer total_cnt = db.getTotalTasksCount(activeUser.getID());
        Integer week_cnt = db.getWeekTasksCount(activeUser.getID());
        Integer day_cnt = db.getTodayTasksCount(activeUser.getID());

        fragmentManager = getSupportFragmentManager();
        // Инициализируем Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AccountHeader.Result headerResult = new AccountHeader()
                .withActivity(this)
                .withHeaderBackground(R.drawable.header)
                .addProfiles(
                        new ProfileDrawerItem().withName(activeUser.getLogin()).withEmail(activeUser.getEmail())
                        //new ProfileDrawerItem().withName("mmalikova").withEmail("mmalikova73@gmail.com")
                )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
                        return false;
                    }
                }).withSelectionListEnabledForSingleProfile(false).build();

        // Инициализируем Navigation Drawer
        drawer = new Drawer();
        drawer  .withActivity(this)
                .withToolbar(toolbar)
                .withActionBarDrawerToggle(true)
                .withAccountHeader(headerResult)
               // .withHeader(R.layout.drawer_header)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.drawer_item_all_tasks).withIcon(FontAwesome.Icon.faw_home).withBadge(total_cnt.toString()).withIdentifier(1),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_today).withIcon(FontAwesome.Icon.faw_calendar).withIdentifier(2),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_week).withIcon(FontAwesome.Icon.faw_leanpub).withIdentifier(3),
                        new DividerDrawerItem(),
                        new PrimaryDrawerItem().withName("Без контекста").withIcon(FontAwesome.Icon.faw_tag));
        for (int i = 0; i< titlesList.length; i++)
        {
            drawer.addDrawerItems(new PrimaryDrawerItem().withName(titlesList[i]).withIcon(FontAwesome.Icon.faw_tag));
        }
        drawer.addDrawerItems(
                new SectionDrawerItem().withName(R.string.drawer_item_settings),
                new SecondaryDrawerItem().withName(R.string.drawer_item_context).withIcon(FontAwesome.Icon.faw_map_marker),
                new SecondaryDrawerItem().withName("Настройки").withIcon(FontAwesome.Icon.faw_cog),
                new SecondaryDrawerItem().withName(R.string.drawer_item_license).withIcon(FontAwesome.Icon.faw_question), //new SecondaryDrawerItem().withName("Соглашение").withIcon(FontAwesome.Icon.faw_question).setEnabled(false)
                new SecondaryDrawerItem().withName("Синхронизация").withIcon(FontAwesome.Icon.faw_refresh), //new SecondaryDrawerItem().withName("Соглашение").withIcon(FontAwesome.Icon.faw_question).setEnabled(false)
                new SecondaryDrawerItem().withName("Выйти").withIcon(FontAwesome.Icon.faw_sign_out) //new SecondaryDrawerItem().withName("Соглашение").withIcon(FontAwesome.Icon.faw_question).setEnabled(false)
                //
                //new SecondaryDrawerItem().withName(R.string.drawer_item_contact).withIcon(FontAwesome.Icon.faw_github).withBadge("12+").withIdentifier(1)
        );
        drawer.withOnDrawerListener(new Drawer.OnDrawerListener() {
            @Override
            public void onDrawerOpened(View drawerView) {
                // Скрываем клавиатуру при открытии Navigation Drawer
                InputMethodManager inputMethodManager = (InputMethodManager) MainActivity.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(MainActivity.this.getCurrentFocus().getWindowToken(), 0);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
            }
        })
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    // Обработка клика
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id, IDrawerItem drawerItem) {

                        if (drawerItem instanceof ProfileDrawerItem){
                            Toast.makeText(MainActivity.this, position, Toast.LENGTH_SHORT).show();

                        }

                        if (drawerItem instanceof Nameable) {

                            int con = 6 + titlesList.length;
                            int set = con + 1;
                            int leg = set + 1;
                            int synq = leg + 1;
                            int out = synq + 1;
                            ///
                            Fragment fragment = null;
                            switch (position) {
                                case 0:
                                    // все задачи
                                    fragment = new TaskList();
                                    //fragment = new TaskTabs();
                                    //
                                    break;
                                case 1:
                                    // сегодня
                                    fragment = new TaskList();
                                    Bundle bundle = new Bundle();
                                    bundle.putString("tag", "today");
                                    fragment.setArguments(bundle);
                                    break;
                                case 2:
                                    // неделя
                                    fragment = new SeparatedFragment();
                                    break;
                                case 4:
                                    // без контекста
                                    fragment = new TaskList();
                                    Bundle bundle1 = new Bundle();
                                    bundle1.putString("tag", "without");
                                    fragment.setArguments(bundle1);
                                    break;

                                default:
                                    if (position == con) {
                                        fragment = new LocationList();
                                    } else if (position == leg) {
                                        String LicenseInfo = GooglePlayServicesUtil.getOpenSourceSoftwareLicenseInfo(getApplicationContext());
                                        AlertDialog.Builder LicenseDialog = new AlertDialog.Builder(MainActivity.this);
                                        LicenseDialog.setTitle("Legal Notices");
                                        LicenseDialog.setMessage(LicenseInfo);
                                        LicenseDialog.show();
                                    } else if (position == set){
                                        fragment = new Settings();
                                    }else if (position == out){
                                        // чистим настройки - удаляем token

                                        SharedPreferences mSettings = getSharedPreferences("mysettings", Context.MODE_PRIVATE);
                                        SharedPreferences.Editor editor = mSettings.edit();
                                        editor.putString("token", "");
                                        editor.apply();

                                        DBWorker db = new DBWorker(MainActivity.this);
                                        db.Exit();
                                        Intent intent = new Intent(MainActivity.this, AuthActivity.class);
                                        startActivity(intent);
                                        MainActivity.this.finish();
                                    }  else if (position == synq){
                                        // todo make only refresh not full synq
                                        WebWorker www = new WebWorker(MainActivity.this);
                                        www.makeSync(activeUser);
                                    }
                                    else {
                                        fragment = new TaskList();
                                        Bundle bundle2 = new Bundle();
                                        bundle2.putString("tag", titlesList[position - 5]);
                                        fragment.setArguments(bundle2);
                                    }
                                    break;
                            }

                            if (fragment != null) {
                                FragmentManager fragmentManager = getSupportFragmentManager();
                                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).addToBackStack(null).commit();

                            } else {
                                Log.e(this.getClass().getName(), "Error. Fragment is not created");
                            }
                        }

                        drawerResult.updateBadge(db.getTotalTasksCount(activeUser.getID()).toString(), 0);

                    }
                })
                .withOnDrawerItemLongClickListener(new Drawer.OnDrawerItemLongClickListener() {
                    @Override
                    // Обработка длинного клика, например, только для SecondaryDrawerItem
                    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id, IDrawerItem drawerItem) {
                        if (drawerItem instanceof SecondaryDrawerItem) {
                            Toast.makeText(MainActivity.this, MainActivity.this.getString(((SecondaryDrawerItem) drawerItem).getNameRes()), Toast.LENGTH_SHORT).show();
                        }
                        return false;
                    }
                });
                drawerResult = drawer.build();

    }




    public void timerSync(){
        Timer myTimer = new Timer(); // Создаем таймер

        myTimer.schedule(new TimerTask() { // Определяем задачу
            @Override
            public void run() {
                Thread myThread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        WebWorker www = new WebWorker(MainActivity.this);
                        www.makeSync(activeUser);
                    }
                });

                myThread.start(); // запускаем
            };
        }, 0L, 900L * 1000);
    }

    public void timerLocation(){



        Timer myTimer = new Timer(); // Создаем таймер

        myTimer.schedule(new TimerTask() { // Определяем задачу
            @Override
            public void run() {
                MyTask task = new MyTask();
                task.execute();
            };
       // }, 0L, 300L * 1000);
        }, 0L, 60L * 1000);
    }
    class MyTask extends AsyncTask<Void, Void, Void> {

        String n;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //запускаем диалог показывающий что ты работаешь во всю
        }

        @Override
        protected Void doInBackground(Void... params) {
            //вот тут пишем весь кошмар и ужас который будет выполнять в отдельном потоке, короче что угодно.
            ContextNotify cn = new ContextNotify(MainActivity.this, mTTS);
            n = cn.NotifyContext();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            Toast.makeText(MainActivity.this, "Проверяем контекст", Toast.LENGTH_SHORT).show();
            mTTS.speak(n, TextToSpeech.QUEUE_FLUSH, null);
            //а здесь мы прячем диалог и заканчиваем работу всех функций которые были запущены в doInBackground()
        }
    }



    @Override
    protected void onDestroy() {


        //Close the Text to Speech Library
        if(mTTS != null) {

            mTTS.stop();
            mTTS.shutdown();
            Log.d("ertert", "TTS Destroyed");
        }
        super.onDestroy();
    }

}
