package com.example.maria.testnavbar.Widget;

import android.content.Intent;
import android.widget.RemoteViewsService;

import com.example.maria.testnavbar.Widget.WidgetRemoteViewsFactory;

public class WidgetService extends RemoteViewsService
{

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent){
        return (new WidgetRemoteViewsFactory(this.getApplicationContext(), intent));
    }

}
