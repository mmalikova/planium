package com.example.maria.testnavbar.Services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import com.example.maria.testnavbar.Activities.MainActivity;
import com.example.maria.testnavbar.Provider.ContextContext;
import com.example.maria.testnavbar.Provider.DBWorker;
import com.example.maria.testnavbar.Provider.PointContext;
import com.example.maria.testnavbar.Provider.TaskContext;
import com.example.maria.testnavbar.Provider.UserContext;
import com.example.maria.testnavbar.R;

import java.util.ArrayList;
import java.util.Locale;

import im.delight.android.location.SimpleLocation;


public class ContextNotify {

    Context cnt;
    Double curLatitude;
    Double curLongtitude;
    SimpleLocation location;
    DBWorker db;
    UserContext user;

    ContextContext curContext;
    int curContextID;

    private TextToSpeech mTTS;

    public static final String APP_PREFERENCES = "mysettings";
    public static final String APP_PREFERENCES_VAR = "curContext";
    private SharedPreferences mSettings;

    private static final int NOTIFICATION_ID = 1000;

    public ContextNotify(Context context, TextToSpeech tts){
        this.cnt = context;
        location = new SimpleLocation(cnt);
        db = new DBWorker(cnt);
        mSettings = cnt.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        user = db.checkUser();
        this.mTTS = tts;

    }

    public String NotifyContext(){

        if (!location.hasLocationEnabled()) {
            SimpleLocation.openSettings(cnt);
        }

        String n = null;
        curLatitude = location.getLatitude();
        curLongtitude = location.getLongitude();

        ArrayList<PointContext> points = new ArrayList<PointContext>();
        points = db.getAllPoints();

        for (int i=0; i<points.size(); i++){
            Double distance = location.calculateDistance(curLatitude, curLongtitude, Double.parseDouble(points.get(i).getLat()), Double.parseDouble(points.get(i).getLng()));
            if (distance < 250){
                curContextID = points.get(i).getContextID();
                curContext = db.getContextByID(curContextID);
                if (curContext.getUserID() == user.getID()){
                    break;
                } else {
                    curContextID = 0;
                }
            }
        }


        // get cnt from settings
        int setCnt = mSettings.getInt(APP_PREFERENCES_VAR, 0);

        if (curContextID > 0 && setCnt > 0 && curContextID!=setCnt){
            n = createNotify();
            SharedPreferences.Editor editor = mSettings.edit();
            editor.putInt(APP_PREFERENCES_VAR, curContextID);
            editor.apply();
        } else if (curContextID > 0 && setCnt == 0){
            n = createNotify();
            SharedPreferences.Editor editor = mSettings.edit();
            editor.putInt(APP_PREFERENCES_VAR, curContextID);
            editor.apply();
        } else if (curContextID==setCnt){
            //
            n = createNotify();
            //
        } else {
            SharedPreferences.Editor editor = mSettings.edit();
            editor.putInt(APP_PREFERENCES_VAR, 0);
            editor.apply();
        }

        if (mTTS != null) {
            mTTS.stop();
            mTTS.shutdown();
        }
        if (n!=null)
            return n;
        else return null;
    }

    public String createNotify() {
        if (curContextID > 0) {
            String remTitle = "";
            ArrayList<TaskContext> taskList = db.getNotifyTasksByContext(curContextID);
            if (taskList.size() == 1) {
                remTitle = taskList.get(0).getText();
            } else {
                if (taskList.size() > 0) {
                    for (int i = 0; i < taskList.size(); i++) {
                        if (i != 0)
                            remTitle += ";   ";
                        remTitle += taskList.get(i).getText();
                    }
                }
            }



            if (remTitle.length()>0){
                Intent notificationIntent = new Intent(cnt, MainActivity.class);
                PendingIntent contentIntent = PendingIntent.getActivity(cnt,0, notificationIntent,PendingIntent.FLAG_CANCEL_CURRENT);
                Notification.Builder builder = new Notification.Builder(cnt);
                Resources res = cnt.getResources();
                builder.setContentIntent(contentIntent)
                        .setSmallIcon(R.mipmap.ic_action_about)
                        .setLargeIcon(BitmapFactory.decodeResource(res, R.mipmap.ic_action_event))
                        .setTicker("ToDO!")
                        .setWhen(System.currentTimeMillis())
                        .setAutoCancel(true)
                        .setContentTitle(curContext.getTitle());
                builder.setContentText(remTitle);
                Notification notification = builder.build();

                long[] vibrate = new long[]{1000, 1000};
                //notification.vibrate = vibrate;
                notification.ledARGB = Color.GREEN;
                notification.ledOffMS = 0;
                notification.ledOnMS = 1;
                notification.flags = notification.flags | Notification.FLAG_SHOW_LIGHTS;

                NotificationManager notificationManager = (NotificationManager) cnt.getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(NOTIFICATION_ID, notification);

            }

            /// try to spich


            if (remTitle.length() >0){
                //mTTS.speak(remTitle, TextToSpeech.QUEUE_FLUSH, null);
                return remTitle;
            }

        }
        return null;
    }







}
