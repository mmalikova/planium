package com.example.maria.testnavbar.Provider;

import java.util.Date;

/**
 * Created by Maria on 25.04.2015.
 */
public class SubtaskContext {

    private int ID;
    private int sID;
    private int taskID;
    private String text;
    private Date lastChange;
    private boolean isChecked = false;

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }



    public SubtaskContext(){}

    public int getID() { return ID; }
    public int getsID() { return sID; }
    public int getTaskID() { return taskID; }
    public String getText() { return text; }
    public Date getLastChange() { return lastChange; }

    public void setID(int ID) { this.ID = ID; }
    public void setsID(int sID) { this.sID = sID; }
    public void setTaskID(int taskID) { this.taskID = taskID; }
    public void setText(String text) { this.text = text; }
    public void setLastChange(Date lastChange) { this.lastChange = lastChange; }
}
