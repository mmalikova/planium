package com.example.maria.testnavbar.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.maria.testnavbar.Provider.ContextContext;
import com.example.maria.testnavbar.Provider.DBWorker;
import com.example.maria.testnavbar.Provider.PointContext;
import com.example.maria.testnavbar.Provider.UserContext;
import com.example.maria.testnavbar.R;
import com.example.maria.testnavbar.RestAPI.WebWorker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


public class Point extends Fragment implements GoogleMap.OnMapLongClickListener,
        GoogleMap.OnMapClickListener, GoogleMap.OnMarkerDragListener {

    private static View view;
    MapView mMapView;
    private GoogleMap googleMap;
    Context context;

    TextView newCoord;
    LatLng newPoint;
    EditText newTitle;

    private DBWorker db;

    Boolean newLocation = true;
    UserContext activeUser;

    PointContext point = new PointContext();

    int pointID;
    int contextID;

    public Point() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = (RelativeLayout) inflater.inflate(R.layout.fragment_point, container, false);
        context = getActivity();
        db = new DBWorker(getActivity());
        activeUser = db.checkUser();

        newTitle = (EditText)view.findViewById(R.id.new_point_description);
        newCoord = (TextView)view.findViewById(R.id.new_lat_lng);
        mMapView = (MapView) view.findViewById(R.id.new_point_mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume();// needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        googleMap = mMapView.getMap();

        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);

        googleMap.setOnMapClickListener(this);
        googleMap.setOnMarkerDragListener(this);


        Bundle bundle = getArguments();
        if (bundle != null) {
            pointID = bundle.getInt("pointID");

            if (pointID!=0) {
                newLocation = false;
                point = db.getPointByID(pointID);
                newTitle.setText(point.getDescription());
                newPoint = new LatLng(Double.parseDouble(point.getLat()), Double.parseDouble(point.getLng()));
                MarkerOptions marker = new MarkerOptions().title(point.getDescription()).position(newPoint);
                marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
                googleMap.addMarker(marker);
                marker.draggable(true);
                googleMap.animateCamera(CameraUpdateFactory.newLatLng(newPoint));
                newCoord.setText(newPoint.toString());
                getActivity().setTitle(point.getDescription());
            }
            contextID = bundle.getInt("contextID");
            if (contextID != 0){
                point.setContextID(contextID);
            }

        }
        else{getActivity().setTitle("Новая точка");}

        return view;
    }

    public void savePointToDB()
    {
        int pointId;
        if (newTitle.getText().length() > 0 && newPoint != null) {
            point.setDescription(newTitle.getText().toString());
            point.setLat(Double.toString(newPoint.latitude));
            point.setLng(Double.toString(newPoint.longitude));

            if (newLocation) {
                pointId = db.addPoint(point); //insert new
            } else {
                pointId = pointID;
                point.setID(pointID);
                if (point.getContextID()>0 && point.getsID()>0){
                    ContextContext ccnt = db.getContextByID(point.getContextID());
                    WebWorker www = new WebWorker(getActivity());
                    www.updatePoint(point, activeUser, ccnt.getsID());
                }
                db.changeTempPoint(point);
            }
            //setProximityAlert(pointId);
        }
    }



    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        try {
            super.onDestroy();
            mMapView.onDestroy();
        } catch (Exception ex){
            Log.e("MapError", ex.toString());
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }


    public void onMapLongClick(LatLng point) {
        googleMap.addMarker(new MarkerOptions().position(point).title(
                point.toString()));
    }

    @Override
    public void onMapClick(LatLng point) {
        newPoint = point;
        newCoord.setText(point.toString());


        googleMap.clear();
        MarkerOptions marker = new MarkerOptions().title("Новая метка")
                .position(point);

        // Changing marker icon
        marker.icon(BitmapDescriptorFactory
                .defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        marker.draggable(true);

        // adding marker
        googleMap.addMarker(marker);

        googleMap.animateCamera(CameraUpdateFactory.newLatLng(point));
    }


    public void onMarkerDrag(Marker marker) {

        //setTitle("Marker " + marker.getId() + " Drag@" + marker.getPosition());
    }


    public void onMarkerDragEnd(Marker marker) {

        //setTitle("Marker " + marker.getId() + " DragEnd");
    }


    public void onMarkerDragStart(Marker marker) {

        //setTitle("Marker " + marker.getId() + " DragStart");
    }

    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.new_element, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        FragmentManager fragmentManager;
        switch (item.getItemId()) {
            case R.id.action_save:
                savePointToDB();
                break;
            case R.id.action_cancel:
                //TODO kill fragment, go to previous
                break;
            default:
                super.onOptionsItemSelected(item);
                break;
        }
        Location yfc = new Location();
        if (contextID != 0){
            Bundle bundle = new Bundle();
            bundle.putInt("contextID", contextID);
            yfc.setArguments(bundle);
        }
        fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, yfc).commit();

        return true;
    }

}
