package com.example.maria.testnavbar.Provider;

import java.util.Date;

public class PointContext {

    private int ID;
    private int contextID;
    private int sID;
    private String description;
    private String lat;
    private String lng;
    private Date lastChange;

    public PointContext() {}

    public int getID() {return this.ID;}
    public int getContextID() { return this.contextID; }
    public int getsID() { return sID;}
    public String getDescription() { return description;}
    public String getLat() { return lat;}
    public String getLng() { return lng;}
    public Date getLastChange() {return lastChange;}

    public void setID(int ID) {this.ID = ID;}
    public void setContextID(int contextID) { this.contextID = contextID; }
    public void setsID(int sID) {this.sID = sID;}
    public void setDescription(String DESC) {this.description = DESC;}
    public void setLat(String lat) { this.lat = lat;}
    public void setLng(String lng) {this.lng = lng;}
    public void setLastChange(Date lastChange) {this.lastChange = lastChange;}
}
