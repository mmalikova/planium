package com.example.maria.testnavbar.RestAPI;


import android.content.Context;
import android.os.AsyncTask;

import com.example.maria.testnavbar.RestAPI.APIWorker;

import com.example.maria.testnavbar.RestAPI.RequestContext;
import com.example.maria.testnavbar.RestAPI.ResponseContext;

import org.json.JSONObject;

public class AsyncRequest extends AsyncTask<RequestContext, Void, ResponseContext> {

    Context context;

    public AsyncRequest(Context context) {
        super();
        this.context = context;
    }

    @Override
    protected void onPreExecute() { super.onPreExecute(); }

    @Override
    protected ResponseContext doInBackground(RequestContext... request) {

        //JsWorker worker = new JsWorker();

        APIWorker worker = new APIWorker();
        ResponseContext response = worker.makeRequest(request[0]);
        return response;
    }

    @Override
    protected void onPostExecute(ResponseContext jsonData) { }
}
