package com.example.maria.testnavbar.Widget;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.example.maria.testnavbar.Provider.DBWorker;
import com.example.maria.testnavbar.Provider.TaskContext;
import com.example.maria.testnavbar.Provider.UserContext;
import com.example.maria.testnavbar.R;
import com.example.maria.testnavbar.Widget.NewAppWidget;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WidgetRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {
    private Context context = null;
    private int appWidgetId;

    public static final String APP_PREFERENCES = "mysettings";
    public static final String APP_PREFERENCES_VAR = "curContext";
    private SharedPreferences mSettings;
    int setCnt;

    private List<TaskContext> widgetList = new ArrayList<TaskContext>();
    private DBWorker dbhelper;

    public WidgetRemoteViewsFactory(Context context, Intent intent) {
        this.context = context;
        appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID);
        Log.d("AppWidgetId", String.valueOf(appWidgetId));
        dbhelper = new DBWorker(this.context);

        mSettings = context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

    }

    private void updateWidgetListView() {

        setCnt = mSettings.getInt(APP_PREFERENCES_VAR, 0);
        // TODO получить текущий контекст и вывести его
        UserContext activeUser = dbhelper.checkUser();

        if (setCnt > 0){
            this.widgetList = dbhelper.getTasksByContext(setCnt);
        } else {
            this.widgetList = dbhelper.getAllTasks(activeUser);
        }
    }

    @Override
    public int getCount() {
        return widgetList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public RemoteViews getViewAt(int position) {
        Log.d("WidgetCreatingView", "WidgetCreatingView");
        RemoteViews remoteView = new RemoteViews(context.getPackageName(), R.layout.item_widget);

        Log.d("Loading", widgetList.get(position).getText());
        remoteView.setTextViewText(R.id.textView5, widgetList.get(position).getText());

        Intent clickIntent = new Intent();
        //clickIntent.putExtra("position", widgetList.get(position).getID());
        clickIntent.putExtra("position", widgetList.get(position).getID());
        remoteView.setOnClickFillInIntent(R.id.textView5, clickIntent);


        return remoteView;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public void onCreate() {
        updateWidgetListView();
    }

    @Override
    public void onDataSetChanged() {

        this.widgetList.clear();
        updateWidgetListView();
        for (int i = 0; i< widgetList.size(); i++){

        }
    }

    @Override
    public void onDestroy() {
        widgetList.clear();
    }
}
