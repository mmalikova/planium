package com.example.maria.testnavbar.RestAPI;

import android.util.Base64;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.URL;

public class APIWorker {

    static InputStream is = null;
    static JSONObject jObj = null;
    static String json = "";

    // конструктор
    public APIWorker() {}

    public ResponseContext makeRequest(RequestContext request){

        ResponseContext response = new ResponseContext();

        try {

            /// set token
            final String username = request.getLogin();
            final String password = request.getPass();

            if (username.length() > 0){
                Authenticator.setDefault(new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password.toCharArray());
                    }
                });
            }



            /// set url
            URL url = new URL(request.getUrl());
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            /*byte[] auth = (username + ":" + password).getBytes();
            String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
            urlConnection.setRequestProperty("Authorization", "Basic " + basic);*/


            /// set method and data if we need
            urlConnection.setRequestMethod(request.getMethod());
            urlConnection.setRequestProperty("Content-Type", "application/json");

            if (request.getJsonData() != null){
                urlConnection.setDoOutput(true);
                urlConnection.setChunkedStreamingMode(0);

                urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

                if (request.getJsonData()!=null) {

                    // set jsondata
                    BufferedWriter out = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream()));
                    out.write(request.getJsonData().toString());
                    out.close();
                }
            }

            System.setProperty("http.keepAlive", "false");

            urlConnection.setReadTimeout(1000*10*1);
            urlConnection.setConnectTimeout(1000*10*1);

            /// read response data
            try {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                response = readStream(in);
            }
            finally {
                response.setResponseCode(urlConnection.getResponseCode());
                urlConnection.disconnect();
            }

            response.setResponseCode(urlConnection.getResponseCode());

        } catch (MalformedURLException ex) { Log.e("Error", ex.toString());}
        catch (IOException ex) {Log.e("Error", ex.toString());}

        return response;
    }


    public ResponseContext readStream(InputStream in){

        String json = "";
        JSONObject jObj = new JSONObject();
        ResponseContext response = new ResponseContext();

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(in, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            in.close();
            json = sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        // пробуем распарсить JSON объект
        try {
            jObj = new JSONObject(json);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        response.setResponse(jObj);
        return response;
    }
}
