package com.example.maria.testnavbar.Provider;

public class LocationContext {

    public int id;
    public int userID;
    public String title;
    public String description;
    public String lat;
    public String lng;

    public LocationContext(){}

    public LocationContext(int aid, int auserID, String atitle, String adesc, String lat, String lng) {
        this.id = aid;
        this.userID = auserID;
        this.title = atitle;
        this.description = adesc;
        this.lat = lat;
        this.lng = lng;
    }

    public LocationContext(int auserID, String atitle, String adesc, String lat, String lng) {
        this.userID = auserID;
        this.title = atitle;
        this.description = adesc;
        this.lat = lat;
        this.lng = lng;
    }

    public int getUserID(){return userID;}
    public int getID() {return id;}
    public String getTitle() {return title;}
    public String getDescription() {return description;}
    public String getLat() {return lat;}
    public String getLng() {return lng;}

    public void setUserID(int uid) {this.userID = uid;}
    public void setTitle(String text) {this.title = text;}
    public void setDescription(String text) {this.description = text;}
    public void setLat(String text) {this.lat = text;}
    public void setLng(String text) {this.lng = text;}
    public void setId (int id) {this.id = id;}

}

