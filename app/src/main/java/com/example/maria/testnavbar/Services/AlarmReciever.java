package com.example.maria.testnavbar.Services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.widget.Toast;

import com.example.maria.testnavbar.Activities.MainActivity;
import com.example.maria.testnavbar.Provider.DBWorker;
import com.example.maria.testnavbar.Provider.TaskContext;
import com.example.maria.testnavbar.R;

public class AlarmReciever extends BroadcastReceiver {


    DBWorker db;

    private static final int NOTIFY_ID = 101;
    @Override
    public void onReceive(Context context, Intent intent) {


        try {
            Integer taskID = Integer.parseInt(intent.getAction());
            db = new DBWorker(context);
            TaskContext task = db.getTaskById(taskID);

            Intent notificationIntent = new Intent(context, MainActivity.class);

            PendingIntent contentIntent = PendingIntent.getActivity(context,
                    0, notificationIntent,
                    PendingIntent.FLAG_CANCEL_CURRENT);


            Resources res = context.getResources();
            Notification.Builder builder = new Notification.Builder(context);

            builder.setContentIntent(contentIntent)
                    .setSmallIcon(R.mipmap.ic_action_about)
                    .setLargeIcon(BitmapFactory.decodeResource(res, R.mipmap.ic_action_event))
                    .setTicker("ToDO!")
                    .setWhen(System.currentTimeMillis())
                    .setAutoCancel(true)
                    .setContentTitle("Напоминание")
                    .setContentText(task.getText()); // Текст уведомления

            //Notification notification = builder.getNotification(); // до API 16
            Notification notification = builder.build();

            long[] vibrate = new long[]{1000, 1000};
            notification.vibrate = vibrate;
            notification.ledARGB = Color.GREEN;
            notification.ledOffMS = 0;
            notification.ledOnMS = 1;
            notification.flags = notification.flags | Notification.FLAG_SHOW_LIGHTS;

            NotificationManager notificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(NOTIFY_ID, notification);
        }
        catch (NumberFormatException e){
            Toast.makeText(context, "Какая-то ошибка свалилась(", Toast.LENGTH_LONG).show();
        }
    }
}
