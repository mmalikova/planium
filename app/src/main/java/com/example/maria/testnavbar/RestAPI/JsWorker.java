package com.example.maria.testnavbar.RestAPI;

import android.util.Base64;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Created by Maria on 15.05.2015.
 */
public class JsWorker {

    static InputStream is = null;
    static JSONObject jObj = null;
    static String json = "";

    // конструктор
    public JsWorker() {}

    // work only for POST with auth
    public ResponseContext addContext(RequestContext req){

        ResponseContext resp = new ResponseContext();
        JSONObject obj;

        DefaultHttpClient client = new DefaultHttpClient();
        HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); //Timeout Limit
        HttpResponse response = null;
        StringEntity se;

        client.getParams().setParameter("http.protocol.content-charset", HTTP.UTF_8);

        String credentials = req.getLogin() + ":" + req.getPass();
        String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

        int status = -1;

        try {

            switch (req.getMethod()){
                case "GET":
                    HttpGet get = new HttpGet(req.getUrl());
                    //se = new StringEntity( req.getJsonData().toString());
                    //se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    get.addHeader("Authorization", "Basic " + base64EncodedCredentials);

                    //get.setEntity(se);
                    response = client.execute(get);
                    break;
                case "POST":
                    HttpPost post = new HttpPost(req.getUrl());
                    se = new StringEntity( req.getJsonData().toString(), "UTF-8");
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json; charset=utf-8"));
                    post.addHeader("Authorization", "Basic " + base64EncodedCredentials);
                    post.addHeader("Content-Encoding", HTTP.UTF_8);

                    post.setEntity(se);

                    response = client.execute(post);
                    break;
                case "PUT":
                    HttpPut put = new HttpPut(req.getUrl());
                    se = new StringEntity( req.getJsonData().toString(), "UTF-8");
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json; charset=utf-8"));
                    put.addHeader("Authorization", "Basic " + base64EncodedCredentials);
                    put.addHeader("Content-Encoding", HTTP.UTF_8);
                    put.setEntity(se);
                    response = client.execute(put);
                    break;
                case "DELETE":
                    MyDelete delete = new MyDelete(req.getUrl());
                    se = new StringEntity( req.getJsonData().toString());
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    delete.addHeader("Authorization", "Basic " + base64EncodedCredentials);
                    delete.setEntity(se);
                    response = client.execute(delete);
                    break;
                default: break;
            }

            if(response!=null){
                status = response.getStatusLine().getStatusCode();
                resp.setResponseCode(status);
                is = response.getEntity().getContent();
            }

            obj = readStream(is);

            resp.setResponse(obj);

            return resp;

        } catch(Exception e) {
            Log.e("NewError!", e.toString());
            return resp;
        }
    }



    public JSONObject readStream(InputStream in){

        String json = "";
        JSONObject jObj = new JSONObject();

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(in, "utf-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            in.close();
            json = sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        // пробуем распарсить JSON объект
        try {
            jObj = new JSONObject(json);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
        return jObj;
    }

    class MyDelete extends HttpPost{
        public MyDelete(String url){
            super(url);
        }
        @Override
        public String getMethod() {
            return "DELETE";
        }
    }

}
