package com.example.maria.testnavbar.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.example.maria.testnavbar.Provider.SubtaskContext;
import com.example.maria.testnavbar.R;

import java.util.ArrayList;


public class SubtaskAdapter  extends ArrayAdapter<SubtaskContext> {
    private final Activity activity;
    //
    private final ArrayList<SubtaskContext> entries;

    // конструктор класса, принимает активность, листвью и массив данных
    public SubtaskAdapter(final Activity a, final int textViewResourceId, final ArrayList<SubtaskContext> entries) {
        super(a, textViewResourceId, entries);
        this.entries = entries;
        activity = a;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        ViewHolder holder;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.item_task_list, parent, false);
            holder = new ViewHolder();
            // инициализируем нашу разметку
            holder.checkBox = (CheckBox) v.findViewById(R.id.checkBox_list_item);
            holder.checkBox.setEnabled(true);
            holder.checkBox.setClickable(true);
            holder.checkBox.setFocusable(true);
            //holder.textView = (TextView) v.findViewById(R.id.textView_list_item_id);


            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        SubtaskContext task = entries.get(position);
        if (task != null) {
            // получаем текст из массива
            holder.checkBox.setText(task.getText());
            if (task.isChecked()){
                holder.checkBox.setChecked(true);
            }
            holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
                    // TODO Auto-generated method stub
                    Toast.makeText(activity, "Check box " + arg0.getText().toString() + " is " + String.valueOf(arg1), Toast.LENGTH_LONG).show();
                    int id=0;
                    for (int i = 0; i<entries.size(); i++) {
                        if (entries.get(i).getText() == arg0.getText().toString() ){
                            id = i;
                        }
                    }
                    entries.get(id).setChecked(true);
                    //entries.remove(id);
                    //notifyDataSetChanged();
                }
            });

            //holder.textView.setText(task.getID());
        }
        return v;
    }

    // для быстроты вынесли в отдельный класс
    private static class ViewHolder {
        public CheckBox checkBox;
        //public TextView textView;
    }
}
