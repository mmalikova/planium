package com.example.maria.testnavbar.Activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.maria.testnavbar.Provider.DBWorker;
import com.example.maria.testnavbar.Provider.UserContext;
import com.example.maria.testnavbar.R;
import com.example.maria.testnavbar.RestAPI.WebWorker;

public class AuthActivity extends ActionBarActivity {

    EditText authLogin;
    EditText authPass;
    Button authBtn;
    TextView authRegLink;










    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        authLogin = (EditText) findViewById(R.id.authLogin);
        authPass = (EditText) findViewById(R.id.authPass);
        authBtn = (Button) findViewById(R.id.authBtn);
        authRegLink = (TextView) findViewById(R.id.authRegLink);

        // if we have active user - go to main activity
        DBWorker db = new DBWorker(this);
        UserContext user = db.checkUser();
        if (user.getID() > 0){
            Intent intent = new Intent(AuthActivity.this, MainActivity.class);
            startActivity(intent);
            this.finish();
        }

        authBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                authorization();
            }
        });

        authRegLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AuthActivity.this, RegisterActivity.class);
                AuthActivity.this.startActivity(intent);
            }
        });


    }

    public void authorization(){
        String email = authLogin.getText().toString();
        String pass = authPass.getText().toString();
        if (email.length() >  0 && pass.length()>0){
            DBWorker db = new DBWorker(this);
            UserContext user = db.authUser(email, pass);
            if (user.getID() > 0){
                Intent intent = new Intent(AuthActivity.this, MainActivity.class);
                startActivity(intent);
                this.finish();
            }
            else {

               /* WebWorker www = new WebWorker(this);

                if (www.hasInternetConnection()) {
                    UserContext newUser = www.getToken(pass, email);
                    if (newUser != null) {
                        if (newUser.getEmail().length() > 0) {
                            newUser = db.registerUser(newUser);
                            //www.makeFullSync(newUser);
                            Intent intent = new Intent(AuthActivity.this, MainActivity.class);
                            startActivity(intent);
                            this.finish();
                        }
                    }
                }
                else {
                    Toast.makeText(getApplicationContext(), "Нет подключения к интернету, повторите попытку позже", Toast.LENGTH_LONG).show();
                }
*/
            }
        }
        else {
            Toast.makeText(getApplicationContext(), "Ошибка в заполнении полей!", Toast.LENGTH_LONG).show();
        }

    }


}
