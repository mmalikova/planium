package com.example.maria.testnavbar.Widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.example.maria.testnavbar.Activities.MainActivity;
import com.example.maria.testnavbar.Provider.ContextContext;
import com.example.maria.testnavbar.Provider.DBWorker;
import com.example.maria.testnavbar.R;

public class NewAppWidget extends AppWidgetProvider {

    final static String ACTION_ON_CLICK = "com.example.maria.testnavbar.Widget.NewAppWidget";
    public final static String ITEM_POSITION = "position";




    @Override
    public void onDeleted(Context context, int[] appWidgetIds)
    {
        super.onDeleted(context, appWidgetIds);
    }

    @Override
    public void onDisabled(Context context)
    {
        super.onDisabled(context);
    }

    @Override
    public void onEnabled(Context context)
    {
        super.onEnabled(context);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);

        if (intent.getAction().equalsIgnoreCase(AppWidgetManager.ACTION_APPWIDGET_OPTIONS_CHANGED)) {
            Intent newTaskIntent = new Intent(context, MainActivity.class);
            newTaskIntent.putExtra("task", -10);
            newTaskIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(newTaskIntent);
        }

        if (intent.getAction().equalsIgnoreCase(AppWidgetManager.ACTION_APPWIDGET_UPDATE)) {
            Toast.makeText(context, "update", Toast.LENGTH_SHORT).show();

            ComponentName thisAppWidget = new ComponentName(context.getPackageName(), getClass().getName());
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            int ids[] = appWidgetManager.getAppWidgetIds(thisAppWidget);
            appWidgetManager.notifyAppWidgetViewDataChanged(ids, R.id.lvList);


        }



        if (intent.getAction().equalsIgnoreCase(ACTION_ON_CLICK)) {
            int itemPos = intent.getIntExtra(ITEM_POSITION, -1);
            if (itemPos != -1) {
                Log.i("receive", Integer.toString(itemPos) );
                Intent detailIntent = new Intent(context, MainActivity.class);
                detailIntent.putExtra("task", itemPos);
                detailIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(detailIntent);
            }
        }
    }



    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {


        for(int i=0;i<appWidgetIds.length;i++)
        {
            updateWidget(context, appWidgetManager, appWidgetIds[i]);
        }
        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }


    static void updateWidget(Context context, AppWidgetManager appWidgetManager,
                             int widgetID) {
        RemoteViews rv = new RemoteViews(context.getPackageName(),
                R.layout.new_app_widget);

        Intent intent = new Intent(context, WidgetService.class);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                widgetID);
        intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));

        String APP_PREFERENCES = "mysettings";
        String APP_PREFERENCES_VAR = "curContext";
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        int setCnt = mSettings.getInt(APP_PREFERENCES_VAR, 0);
        DBWorker db = new DBWorker(context);

        if (setCnt>0){
            ContextContext curCnt = db.getContextByID(setCnt);
            rv.setTextViewText(R.id.tvUpdate, curCnt.getTitle());
        } else {
            rv.setTextViewText(R.id.tvUpdate, "Все задачи");
        }
        rv.setRemoteAdapter(R.id.lvList, intent);


        Intent clickIntent=new Intent(context, NewAppWidget.class);

        clickIntent.setAction(ACTION_ON_CLICK);
        clickIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, widgetID);

        PendingIntent pi=PendingIntent.getBroadcast(context, widgetID , clickIntent,  PendingIntent.FLAG_UPDATE_CURRENT);
        rv.setPendingIntentTemplate(R.id.lvList, pi);

        // new
        Intent newTask = new Intent(context, NewAppWidget.class);
        newTask.setAction(AppWidgetManager.ACTION_APPWIDGET_OPTIONS_CHANGED);
        newTask.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS,
                new int[] { widgetID });
        pi = PendingIntent.getBroadcast(context, widgetID, newTask, PendingIntent.FLAG_UPDATE_CURRENT);
        rv.setOnClickPendingIntent(R.id.imageButton3, pi);

        //refresh
        Intent refresh = new Intent(context, NewAppWidget.class);
        refresh.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        refresh.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, new int[] { widgetID });
        pi = PendingIntent.getBroadcast(context, widgetID, refresh, PendingIntent.FLAG_UPDATE_CURRENT);
        rv.setOnClickPendingIntent(R.id.imageButton4, pi);

        appWidgetManager.updateAppWidget(widgetID, rv);
    }


}