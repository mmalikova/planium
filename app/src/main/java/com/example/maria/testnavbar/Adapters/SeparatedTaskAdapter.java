package com.example.maria.testnavbar.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.maria.testnavbar.Provider.SeparatedTask;
import com.example.maria.testnavbar.R;

import java.util.ArrayList;


public class SeparatedTaskAdapter extends ArrayAdapter<SeparatedTask> {
    private final Activity activity;
    //
    private final ArrayList<SeparatedTask> entries;

    // конструктор класса, принимает активность, листвью и массив данных
    public SeparatedTaskAdapter(final Activity a, final int textViewResourceId, final ArrayList<SeparatedTask> entries) {
        super(a, textViewResourceId, entries);
        this.entries = entries;
        activity = a;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        ViewHolder holder;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.item_separated_list, parent, false);
            holder = new ViewHolder();
            // инициализируем нашу разметку
            holder.checkBox = (CheckBox) v.findViewById(R.id.checkBox_task);
            holder.separator = (TextView) v.findViewById(R.id.separator);
            //holder.textView = (TextView) v.findViewById(R.id.textView_list_item_id);

            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        SeparatedTask task = entries.get(position);
        if (task != null) {
            // получаем текст из массива

            if (task.getIsChecked() != 1){
                holder.checkBox.setText(task.getTitle());
                holder.checkBox.setChecked(false);
            }
            else{
                holder.checkBox.setText(task.getTitle());
                holder.checkBox.setChecked(true);
            }
            if (task.getNeedSeparator() ==1 ) {
                holder.separator.setText(task.getSeparatorTitle());
                holder.separator.setVisibility(View.VISIBLE);
            } else {
                holder.separator.setVisibility(View.GONE);
            }
        }
        return v;
    }

    // для быстроты вынесли в отдельный класс
    private static class ViewHolder {
        public CheckBox checkBox;
        public TextView separator;
        //public TextView textView;
    }
}