package com.example.maria.testnavbar.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.maria.testnavbar.Provider.PointContext;
import com.example.maria.testnavbar.R;

import java.util.ArrayList;


public class PointListAdapter extends ArrayAdapter<PointContext> {

    private final FragmentActivity activity;
    //
    private final ArrayList<PointContext> entries;

    // конструктор класса, принимает активность, листвью и массив данных
    public PointListAdapter(final Activity a, final int textViewResourceId, final ArrayList<PointContext> entries) {
        super(a, textViewResourceId, entries);
        this.entries = entries;
        activity = (FragmentActivity)a;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        ViewHolder holder;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.item_point_list, parent, false);
            holder = new ViewHolder();
            // инициализируем нашу разметку
            holder.desc = (TextView) v.findViewById(R.id.pointDesc);
            holder.coord = (TextView) v.findViewById(R.id.pointCoord);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        PointContext point = entries.get(position);
        if (point != null) {
            // получаем текст из массива
            holder.desc.setText(point.getDescription());
            holder.coord.setText("Lat/lng: " + point.getLat() + "/" + point.getLat());
        }
        return v;
    }

    // для быстроты вынесли в отдельный класс
    private static class ViewHolder {
        public TextView desc;
        public TextView coord;
    }
}
