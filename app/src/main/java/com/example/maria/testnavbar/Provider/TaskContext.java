package com.example.maria.testnavbar.Provider;

import java.util.Date;

public class TaskContext {

    private int ID;
    private int sID;
    private int userID;
    private int checked;
    private String text;
    private Date dueDate;
    private Date notice;
    private int repeatID;
    private int locationNotice;
    private Date lastChange;

    public int getlChSec() {
        return lChSec;
    }

    public void setlChSec(int lChSec) {
        this.lChSec = lChSec;
    }

    private int lChSec;

    public TaskContext(){}

    /*public TaskContext(int anInt, String string, String string1, int UserID) {
        this.ID = anInt;
        this.NoteTitle = string;
        this.NoteText = string1;
        this.userID = UserID;
    }

    public TaskContext(String string, String string1, int UserID) {
        this.NoteTitle = string;
        this.NoteText = string1;
        this.userID = UserID;
    }

    public TaskContext(String string, String string1, int UserID, String[] all, boolean[] noteL) {
        this.NoteTitle = string;
        this.NoteText = string1;
        this.userID = UserID;
        this.allLocations = all;
        this.noteLocations = noteL;
    }

    public TaskContext(int anInt, String string, String string1, int UserID, String[] all, boolean[] noteL) {
        this.id = anInt;
        this.NoteTitle = string;
        this.NoteText = string1;
        this.userID = UserID;
        this.allLocations = all;
        this.noteLocations = noteL;
    }*/


    public int getID() {return this.ID;}
    public int getsID() {return this.sID;}
    public int getUserID() {return this.userID;}
    public int getChecked() {return this.checked;}
    public String getText() {return this.text;}
    public Date getDueDate() {return this.dueDate;}
    public Date getNotice() {return this.notice;}
    public int getRepeatID() {return this.repeatID;}
    public int getLocationNotice() {return this.locationNotice;}
    public Date getLastChange() {return this.lastChange;}

    public void setID(int ID) {this.ID = ID;}
    public void setsID(int SID) {this.sID = SID;}
    public void setUserID(int USERID) {this.userID = USERID;}
    public void setChecked(int CHECKED) {this.checked = CHECKED;}
    public void setText(String TEXT) {this.text = TEXT;}
    public void setDueDate(Date DUE) {this.dueDate = DUE;}
    public void setNotice(Date NOTICE) {this.notice = NOTICE;}
    public void setRepeatID(int REPEATID) {this.repeatID = REPEATID;}
    public void setLocationNotice(int LOCATIONNOTICE) {this.locationNotice = LOCATIONNOTICE;}
    public void setLastChange(Date LASTCHANGE) { this.lastChange = LASTCHANGE;}


    /*public ArrayList<String> getNoteLocationTitles(){
        ArrayList<String> list = new ArrayList<String>();
        for(int i =0; i<allLocations.length; i++){
            if (noteLocations[i])
                list.add(allLocations[i]);
        }
        return list;
    }*/
}
