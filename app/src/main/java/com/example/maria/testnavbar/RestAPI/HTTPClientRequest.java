package com.example.maria.testnavbar.RestAPI;

import android.content.Context;
import android.os.AsyncTask;


public class HTTPClientRequest extends AsyncTask<RequestContext, Void, ResponseContext> {

    Context context;

    public HTTPClientRequest(Context context) {
        super();
        this.context = context;
    }

    @Override
    protected void onPreExecute() { super.onPreExecute(); }

    @Override
    protected ResponseContext doInBackground(RequestContext... request) {

        JsWorker worker = new JsWorker();
        ResponseContext response = worker.addContext(request[0]);
        return response;
    }

    @Override
    protected void onPostExecute(ResponseContext jsonData) { }
}
